VOTCA-CTP is a library containing functionality for electronic state calculation
   using DFT and coupling to classical environments and for charge transport simulations
   on molecular dynamics structures.

.. Further information on VOTCA can be found at http://www.votca.org

The development of VOTCA is mainly funded by academic research grants.
If you use this package, please cite the following VOTCA papers:

-  | *Long-range embedding of molecular ions and excitations in a polarizable molecular environment*,
   | C. Poelking, D. Andrienko,
   | `J. Chem. Theo. Comp. 12, 4516-4523
     (2016) <https://doi.org/10.1021/acs.jctc.6b00599>`__.

-  | *Modeling of spatially correlated energetic disorder in organic semiconductors*,
   | P. Kordt, D. Andrienko,
   | `J. Chem. Theory Comput., 12, 36-40
     (2016) <https://doi.org/10.1021/acs.jctc.5b00764>`__.

-  | *Microscopic simulations of charge transport in disordered organic semiconductors*,
   | V. Ruehle, A. Lukyanov, F. May, M. Schrader, T. Vehoff, J. Kirkpatrick, B. Baumeier and D. Andrienko,
   | `J. Chem. Theo. Comp. 7, 3335-3345
     (2011) <https://doi.org/10.1021/ct200388s>`__.

-  | *Extracting nondispersive charge carrier mobilities of organic semiconductors from simulations of small systems*,
   | A. Lukyanov, D. Andrienko,
   | `Phys. Rev. B, 82, 193202
     (2010) <https://doi.org/10.1103/PhysRevB.82.193202>`__.

-  | *Density-functional based determination of intermolecular charge transfer properties for large-scale morphologies*,
   | B. Baumeier, J. Kirkpatrick, and D. Andrienko,
   | `Phys. Chem. Chem. Phys. 12, 11103
     (2010) <https://doi.org/10.1039/C002337J>`__.

-  | *Versatile Object-oriented Toolkit for Coarse-graining
     Applications*,
   | V.Ruehle, C. Junghans, A. Lukyanov, K. Kremer, and D. Andrienko,
   | `J. Chem. Theo. Comp. 5 (12), 3211
     (2009) <http://dx.doi.org/10.1021/ct900369w>`__.
     
-  | *An approximate method for calculating transfer integrals based on the ZINDO Hamiltonian*,
   | J. Kirkpatrick,
   | `Int. J. Quantum Chem. 108 (1), 51-56
     (2007) <https://doi.org/10.1002/qua.21378>`__.

.. In case of questions, please post them in the google discussion group
.. for VOTCA at: http://groups.google.com/group/votca

.. You can contact the VOTCA Development Team at devs@votca.org.

License:

Copyright 2009-2022 The VOTCA Development Team (http://www.votca.org)


Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

::

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
