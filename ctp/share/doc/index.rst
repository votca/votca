***************
Electrostatics
***************

.. toctree::
   :maxdepth: 1
   :hidden:
   
   energetic_disorder.rst
   ewald.rst
   thole.rst
   gdma.rst

A short intro to electrostatics.

********************
Kinetic Monte Carlo
********************

.. toctree::
   :maxdepth: 1
   :hidden:

   kmc.rst
   
**********************
Electronic couplings
**********************
.. toctree::
   :maxdepth: 1
   :hidden:
   
   dft.rst
   moo.rst
