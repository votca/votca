[sec:dft]

The calculation of one electronic coupling element requires the overlap
matrix of atomic orbitals :math:`\matr{\mathcal{S}}`, the expansion
coefficients for monomer
:math:`\vctr{\boldsymbol{\lambda}}_{(k)} = \{ \lambda_\alpha^{(k)}\}`
and dimer orbitals :math:`\vctr{D}_{(n)} = \{ D^{(n)}_{\alpha} \}`, as
well as the orbital energies :math:`E_{n}` of the dimer are required as
input. In practical situations, performing self-consistent
quantum-chemical calculations for each individual monomer and one for
the dimer to obtain this input data is extremely demanding. Several
simplifications can be made to reduce the computational effort, such as
using non-Counterpoise basis sets for the monomers (thereby decoupling
the monomer calculations from the dimer run) and performing only a
single SCF step in a dimer calculation starting from an initial guess
formed from a superposition of monomer orbitals. This ”noCP+noSCF”
variant of is shown in (a) and recommended for production runs. A
detailed comparative study of the different variants can be found
in :raw-latex:`\cite{baumeier_density-functional_2010}`.

The code currently contains supports evaluation of transfer integrals
from quantum-chemical calculations performed with the , , and packages.
The interfacing procedure consists of three main steps: generation of
input files for monomers and dimers, performing the actual
quantum-chemical calculations, and calculating the transfer integrals.

.. _sec:edft:

Monomer calculations
~~~~~~~~~~~~~~~~~~~~

First, and a need to be generated from the atomistic topology and
trajectory and written to the file. Then the parallel manages the
calculation of the monomer properties required for the determination of
electronic coupling elements. Specifically, the individual steps it
performs are:

#. Creation of a job file containing the list of molecules to be
   calculated with DFT

#. Running of all jobs in job file which includes

   -  creating the input files for the DFT calculation (using the
      package specified in ) in the directory

      ::

         OR_FILES/package/frame_F/mol_M

      where F is the index of the frame in the trajectory, M is the
      index of a molecule in this frame,

   -  executing the DFT run, and

   -  after completion of this run, parsing the output (number of
      electrons, basis set, molecular orbital expansion coefficients),
      and saving it in compressed form to

      ::

          OR_FILES/molecules/frame_F/molecule_M.orb 

.. _sec:idft:

Calculating the transfer integrals
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After the momomer calculations have been completed successfully, the
respective runs for dimers from the neighborlist can be performed using
the parallel , which manages the DFT runs for the hopping pairs and
determines the coupling element using . Again, several steps are
required:

#. Creation of a job file containing the list of pairs to be calculated
   with DFT

#. Running of all jobs in job file which includes

   -  creating the input files (including the merged guess for a noSCF
      calculation, if requested) for the DFT calculation (using the
      package specified in ) in the directory

      ::

         OR_FILES/package/frame_F/pair_M_N

      where M and N are the indices of the molecules in this pair,

   -  executing the DFT run, and

   -  after completion of this run, parsing the output (number of
      electrons, basis set, molecular orbital expansion coefficients and
      energies, atomic orbital overlap matrix), and saving the pair
      information in compressed form to

      ::

          OR_FILES/pairs/frame_F/pair_M_N.orb 

   -  loading the monomer orbitals from the previously saved .orb files.

   -  calculating the coupling elements and write them to the job file

#. Reading the coupling elements from the job file and saving them to
   the file
