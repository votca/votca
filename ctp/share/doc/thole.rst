.. _sec:thole_model:

Induction interactions
======================

If we in addition to the permanent set of multipole moments
:math:`\{Q_t^a\}` allow for induced moments :math:`\{\Delta Q_t^a\}` and
penalize their generation with a bilinear form (giving rise to a
strictly positive contribution to the energy),

.. math::

   \begin{aligned}
   U_\textrm{int} &=\frac{1}{2} \sum_A \Delta Q_t^a \eta_{tt'}^{aa'} \Delta Q_{t'}^{a'},\end{aligned}

It can be shown that the induction contribution to the site energy
evaluates to an expression where all interactions between induced
moments have cancelled out, and interactions between permanent and
induced moments are scaled down by
:math:`1/2` :raw-latex:`\cite{stone_theory_1997}`:

.. math::

   \begin{aligned}
   U_{pu} = \frac{1}{2} \sum_A \sum_{B > A} \left[ \Delta Q_t^a T_{tu}^{ab} Q_u^b + \Delta Q_t^b T_{tu}^{ab} Q_u^a \right].
   \label{equ:u_pu}\end{aligned}

 This term can be viewed as the second-order (induction) correction to
the molecular interaction energy. The sets of :math:`\{Q_t^a\}` are
solved for self-consistently via

.. math::

   \begin{aligned}
   \Delta Q_t^a = - \sum_{B \neq A} \alpha_{tt'}^{aa'} T_{t'u}^{a'b} (Q_u^b + \Delta Q_u^b),
   \label{equ:self_consistent_dQ}\end{aligned}

 where the polarizability tensors :math:`\alpha_{tt'}^{aa'}` are given
by the inverse of :math:`\eta_{tt'}^{aa'}`.

With eqs. \ `[equ:self_consistent_dQ] <#equ:self_consistent_dQ>`__
and \ `[equ:u_pu] <#equ:u_pu>`__ we have at hand expressions that allow
us to compute the induction energy contribution to site energies in an
iterative manner based on a set of molecular distributed multipoles
:math:`\{Q_t^a\}` and polarizabilities :math:`\{\alpha_{tt'}^{aa'}\}`.
We have drafted in the previous section how to obtain the former from a
wavefunction decomposition or fitting scheme (GDMA, CHELPG). The
:math:`\{\alpha_{tt'}^{aa'}\}` can be derived formally (or rather: read
off) from a perturbative expansion of the molecular interaction. In this
work we make use of the Thole
model :raw-latex:`\cite{thole_molecular_1981, van_duijnen_molecular_1998}`
as a semi-empirical approach to obtain the sought-after point
polarizabilities in the local dipole approximation, that is,
:math:`[\alpha_{tt'}^{aa'}] = \alpha_{tt'}^{aa'} \delta_{t \beta} \delta_{t'\beta} \delta_{aa'}`,
where :math:`\beta \epsilon \{x,y,z\}` references the dipole-moment
component.

The Thole model is based on a modified dipole-dipole interaction, which
can be reformulated in terms of the interaction of smeared charge
densities. This has been shown to be necessary due to the divergent
head-to-tail dipole-dipole interaction that otherwise results at small
interseparations on the
Å scale :raw-latex:`\cite{applequist_atom_1972, thole_molecular_1981, van_duijnen_molecular_1998}`.
Smearing out the charge distribution mimics the nature of the QM
wavefunction, which effectively guards against this unphysical
polarization catastrophe. Since the point dipoles however only react
individually to the external field, any correlation effects as were
still accounted for in the :math:`\{\alpha_{tt'}^{aa'}\}` are lost,
except perhaps those correlations that are due to the mere classical
field interaction.

The smearing of the nuclei-centered multipole moments is obtained via a
fractional charge density :math:`\rho_f(\vec{u})` which should be
normalized to unity and fall off rapidly as of a certain radius
:math:`\vec{u} = \vec{u}(\vec{R})`. The latter is related to the
physical distance vector :math:`\vec{R}` connecting two interacting
sites via a linear scaling factor that takes into account the magnitude
of the isotropic site polarizabilities :math:`\alpha^a`. This isotropic
fractional charge density gives rise to a modified potential

.. math::

   \begin{aligned}
    \phi(u) = -\frac{1}{4\pi\varepsilon_0} \int \limits_{0}^{u} \! 4\pi u' \rho(u') d\!u' 
    \label{equ:mod_potential}\end{aligned}

 We can relate the multipole interaction tensor :math:`T_{ij \dots}`
(this time in Cartesian coordinates) to the fractional charge density in
two steps: First, we rewrite the tensor in terms of the scaled distance
vector :math:`\vec{u}`,

.. math::

   \begin{aligned}
    T_{ij \dots }(\vec{R}) = f(\alpha^a \alpha^b) \ t_{ij \dots}(\vec{u}(\vec{R},\alpha^a \alpha^b)),\end{aligned}

 where the specific form of :math:`f(\alpha^a \alpha^b)` results from
the choice of :math:`u(\vec{R},\alpha^a \alpha^b)`. Second, we demand
that the smeared interaction tensor :math:`t_{ij \dots}` is given as
usual by the appropriate derivative of the potential in
eq. \ `[equ:mod_potential] <#equ:mod_potential>`__,

.. math::

   \begin{aligned}
    t_{ij \dots}(\vec{u}) = - \partial_{u_i} \partial_{u_j} \dots \phi(\vec{u}).\end{aligned}

 It turns out that for a suitable choice of :math:`\rho_f(\vec{u})`, the
modified interaction tensors can be rewritten in such a way that powers
:math:`n` of the distance :math:`R = |\vec{R}|` are damped with a
damping function
:math:`\lambda_n(\vec{u}(\vec{R}))` :raw-latex:`\cite{ren_polarizable_2003}`.

There is a large number of fractional charge densities
:math:`\rho_f(\vec{u})` that have been tested for the purpose of giving
best results for the molecular polarizability as well as interaction
energies. Note how a great advantage of the Thole model is the
exceptional transferability of the atomic polarizabilities to compounds
not used for the fitting
procedure :raw-latex:`\cite{van_duijnen_molecular_1998}`. In fact, for
most organic molecules, a fixed set of atomic polarizabilities
(:math:`\alpha_C = 1.334`, :math:`\alpha_H = 0.496`,
:math:`\alpha_N = 1.073`, :math:`\alpha_O = 0.873`,
:math:`\alpha_S = 2.926` Å\ :math:`^3`) based on atomic elements yields
satisfactory results.

VOTCA implements the Thole model with an exponentially-decaying
fractional charge density

.. math::

   \begin{aligned}
    \rho(u) = \frac{3a}{4\pi} \exp(-au^3),\end{aligned}

 where
:math:`\vec{u}(\vec{R},\alpha^a \alpha^b) = \vec{R} / (\alpha^a \alpha^b)^{1/6}`
and the smearing exponent :math:`a=0.39` (which can however be changed
from the program options), as used in the AMOEBA force
field :raw-latex:`\cite{ren_polarizable_2003}`.

Even though the Thole model performs very well for many organic
compounds with only the above small set of element-based
polarizabilities, conjugated molecules may require a more intricate
parametrization. The simplest approach is to resort to scaled
polarizabilities to match the effective molecular polarizable volume
:math:`V \sim \alpha_{x} \alpha_{y} \alpha_{z}` as predicted by QM
calculations (here :math:`\alpha_x, \alpha_y, \alpha_z` are the
eigenvalues of the molecular polarizability tensor). The assists with
this task, it self-consistently calculates the Thole polarizability for
an input mps-file and optimizes (if desired) the atomic polarizabilities
in the above simple manner.

.. raw:: latex

   \votcacommand{Generate Thole-type polarizabilites for a segment}{\cmdmolpol}

The electrostatic and induction contribution to the site energy is
evaluated by the . Atomistic partial charges for charged and neutral
molecules are taken from mps-files (extended GDMA format) specified in .
Note that, in order to speed up calculations for both methods, a cut-off
radius (for the molecular centers of mass) can be given in . Threaded
execution is advised.

.. raw:: latex

   \votcacommand{Electrostatic and induction corrections}{\cmdemlt}

Furthermore available are , which extends to allow for an electrostatic
buffer layer (loosely related to the z-buffer in OpenGL, hence the name)
and anisotropic point polarizabilities. For the interaction energy of
charged clusters of any user-defined composition (Frenkel states, CT
states, ...), can be used.

.. raw:: latex

   \votcacommand{Interaction energy of charged molecular clusters embedded in a molecular environment}{\cmdxqmult}
