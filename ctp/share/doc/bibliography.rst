Bibliography
============

.. [Ruehle:2009.a] V. Rühle, C. Junghans, A. Lukyanov, K. Kremer, and D. Andrienko, Versatile Object-oriented Toolkit for Coarse-graining Applications, J. Chem. Theor. Comp., 2009, 5(12):3211-3223, https://dx.doi.org/10.1021/ct900369w
