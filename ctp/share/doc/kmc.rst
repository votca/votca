.. _sec:kmc:

Master equation
===============

Having determined the list of conjugated segments (hopping sites) and
charge transfer rates between them, the next task is to solve the master
equation which describes the time evolution of the system

.. math::

   \label{equ:master}
   \frac{\partial P_\alpha}{\partial t} = \sum_{\beta} P_\beta \Omega_{\beta \alpha} - 
   \sum_{\beta} P_\alpha \Omega_{\alpha \beta},

 where :math:`P_\alpha` is the probability of the system to be in a
state :math:`\alpha` at time :math:`t` and :math:`\Omega_{\alpha \beta}`
is the transition rate from state :math:`\alpha` to state :math:`\beta`.
A state :math:`\alpha` is specified by a set of site occupations,
:math:`\left\{ \alpha_i \right\}`, where :math:`\alpha_i = 1 (0)` for an
occupied (unoccupied) site :math:`i`, and the matrix
:math:`\hat{\Omega}` can be constructed from rates :math:`\omega_{ij}`.

Kinetic Monte Carlo
-------------------

The solution of is be obtained by using kinetic Monte Carlo (KMC)
methods. KMC explicitly simulates the dynamics of charge carriers by
constructing a Markov chain in state space and can find both stationary
and transient solutions of the master equation. The main advantage of
KMC is that only states with a direct link to the current state need to
be considered at each step. Since these can be constructed solely from
current site occupations, extensions to multiple charge carriers
(without the mean-field approximation), site-occupation dependent rates
(needed for the explicit treatment of Coulomb interactions), and
different types of interacting particles and processes, are
straightforward. To optimize memory usage and efficiency, a combination
of the variable step size method :raw-latex:`\cite{bortz_new_1975}` and
the first reaction method is implemented.

To obtain the dynamics of charges using KMC, the program executes a
specific after reading its options (charge carrier type, runtime, numer
of carriers etc.) from .

.. raw:: latex

   \votcacommand{KMC for a single carrier in periodic boundary conditions}{\cmdkmcsin}

.. raw:: latex

   \votcacommand{KMC for multiple carriers of the same type in periodic boundary conditions}{\cmdkmc}
