.. _sec:eanalyze:

Spatial correlations of energetic disorder
==========================================

Long-range, e.g. electrostatic and polarization, interactions often
result in spatially correlated
disorder :raw-latex:`\cite{dunlap_charge-dipole_1996}`, which affects
the onset of the mobility-field (Poole-Frenkel)
dependence :raw-latex:`\cite{derrida_velocity_1983,novikov_essential_1998,nagata_atomistic_2008}`.

To quantify the degree of correlation, one can calculate the spatial
correlation function of :math:`E_i` and :math:`E_j` at a distance
:math:`r_{ij}`

.. math::

   \label{equ:cf}
   C(r_{ij}) = \frac{  \langle \left( E_i-\langle E\rangle \right)
                      \left( E_j-\langle E\rangle \right)\rangle}
                      {\langle\left( E_i -\langle E\rangle \right)^2\rangle},

 where :math:`\langle E\rangle` is the average site energy.
:math:`C(r_{ij})` is zero if :math:`E_i` and :math:`E_j` are
uncorrelated and :math:`1` if they are fully correlated. For a system of
randomly oriented point dipoles, the correlation function decays as
:math:`1/r` at large distances :raw-latex:`\cite{novikov_cluster_1995}`.

For systems with spatial correlations, variations in site energy
differences, :math:`\Delta E_{ij}`, of pairs of molecules from the
neighbor list are smaller than variations in site energies, :math:`E_i`,
of all individual molecules. Since only neighbor list pairs affect
transport, the distribution of :math:`\Delta E_{ij}` rather than that of
individual site energies, :math:`E_i`, should be used to characterize
energetic disorder.

Note that the calculator takes into account all contributions to the
site energies
