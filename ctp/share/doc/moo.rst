.. _sec:izindo:

Semiempirical
=============

An approximate method based on Zerner’s Intermediate Neglect of
Differential Overlap (ZINDO) has been described in
Ref. :raw-latex:`\cite{kirkpatrick_approximate_2008}`. This
semiempirical method is substantially faster than first-principles
approaches, since it avoids the self-consistent calculations on each
individual monomer and dimer. This allows to construct the matrix
elements of the ZINDO Hamiltonian of the dimer from the weighted overlap
of molecular orbitals of the two monomers. Together with the
introduction of rigid segments, only a single self-consistent
calculation on one isolated conjugated segment is required. All relevant
molecular overlaps can then be constructed from the obtained molecular
orbitals.

The main advantage of the molecular orbital overlap (MOO) library is
fast evaluation of electronic coupling elements. Note that MOO is based
on the ZINDO Hamiltonian which has limited applicability. The general
advice is to first compare the accuracy of the MOO method to the
DFT-based calculations.

MOO can be used both in a and as an of .

Since MOO constructs the Fock operator of a dimer from the molecular
orbitals of monomers by translating and rotating the orbitals of , the
optimized geometry of all and the coefficients of the molecular orbitals
are required as its input in addition to the state file () with the .
Coordinates are stored in files with four columns, first being the atom
type and the next three atom coordinates. This is a standard ``xyz``
format without a header. Note that the atom order in the files can be
different from that of the mapping files. The correspondence between the
two is established in the file.

.. raw:: latex

   \attention{Izindo requires the specification of orbitals for hole and electron transport in \xmlcsg. They are the HOMO and LUMO respectively and can be retrieved from the \texttt{log} file from which the \orb file is generated. The number of \texttt{alpha electrons} is the HOMO, the LUMO is HOMO+1 }

The calculated transfer integrals are immediately saved to the file.
