.. _sec:site_energies:

Site energies
=============

A charge transfer reaction between molecules :math:`i` and :math:`j` is
driven by the site energy difference, :math:`\Delta E_{ij} = E_i - E_j`.
Since the transfer rate, :math:`\omega_{ij}`, depends exponentially on
:math:`\Delta E_{ij}` (see ) it is important to compute its distribution
as accurately as possible. The total site energy difference has
contributions due to , , polarization effects, and differences. In what
follows we discuss how to estimate these contributions by making use of
first-principles calculations and polarizable force-fields.

.. _sec:ext_field:

External field
--------------

The contribution to the total site energy difference due to an external
electric field :math:`\vec{F}` is given by
:math:`\Delta E_{ij}^\text{ext} = q {\vec{F} \cdot \vec{r}_{ij}}`, where
:math:`q=\pm e` is the charge and
:math:`\vec{r}_{ij} = \vec{r}_i  - \vec{r}_j` is a vector connecting
molecules :math:`i` and :math:`j`. For typical distances between small
molecules, which are of the order of :math:`1\,\unit{nm}`, and moderate
fields of :math:`F<10^8\,\unit{V/m}` this term is always smaller than
:math:`0.1\, \unit{eV}`.

.. _sec:internal_energy:

Internal energy
---------------

The contribution to the site energy difference due to different internal
energies (see ) can be written as

.. math::

   \Delta E_{ij}^\text{int}=
   \Delta U_i - \Delta U_j = \left( U_{i}^{cC}-U_{i}^{nN}\right) - \left( U_{j}^{cC}-U_{j}^{nN}\right) \, ,
   \label{equ:conformational}

 where :math:`U_{i}^{cC(nN)}` is the total energy of molecule :math:`i`
in the charged (neutral) state and geometry. :math:`\Delta U_{i}`
corresponds to the adiabatic ionization potential (or electron affinity)
of molecule :math:`i`, as shown in . For one-component systems and
negligible conformational changes :math:`\Delta E_{ij}^\text{int}=0`,
while it is significant for donor-acceptor systems.

Internal energies determined using quantum-chemistry need to be
specified in . The values are written to the using the calculator (see
also ):
