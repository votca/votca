cmake_minimum_required(VERSION 3.1)

project(votca)

set(PROJECT_VERSION "2023.1")

# Cmake modules/macros are in a subdirectory to keep this file cleaner
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CXX_FLAGS)
  #release comes with -O3 by default
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CXX_FLAGS)

#by default enable openmp parallelization (useful for Eigen library)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")

include(GNUInstallDirs)
########################################################################
# User input options                                                   #
########################################################################

option(BUILD_TOOLS "Build tools" ON)
option(WITH_SQLITE3 "SQLite3 is needed for ctp" ON)

option(BUILD_CSG "Build csg" ON)
option(BUILD_CSG_MANUAL "Build csg manual" OFF)
option(BUILD_CSG_TUTORIALS "Build csg tutorials" OFF)

option(BUILD_CTP "Build ctp" ON)
option(BUILD_CTP_MANUAL "Build ctp manual" OFF)

option(ENABLE_TESTING "Build and copy testing stuff" OFF)
if(ENABLE_TESTING)
  enable_testing()
endif()

option(BUILD_SHARED_LIBS "Build shared libs" ON)

option(ENABLE_COVERAGE_BUILD "Do a coverage build" OFF)
if(ENABLE_COVERAGE_BUILD)
    message(STATUS "Enabling coverage build")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} --coverage -O0")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage -O0")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} --coverage")
endif()

option(ENABLE_RPATH_INJECT "Inject link and install libdir into executables" OFF)
if(ENABLE_RPATH_INJECT)
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH ON)
  set(CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_FULL_LIBDIR})
endif(ENABLE_RPATH_INJECT)

set(VOTCA_SPHINX_DIR "${PROJECT_BINARY_DIR}/sphinx" CACHE PATH "Path for Sphinx sources")
file(MAKE_DIRECTORY ${VOTCA_SPHINX_DIR})
set(VOTCA_SPHINX_OUTPUT_DIR "${PROJECT_BINARY_DIR}/sphinx.html" CACHE PATH "Path for Sphinx output")
set(VOTCA_XML_PARSER "${PROJECT_SOURCE_DIR}//share/doc/extract_xml_metadata.py")

########################################################################
#Find external packages
########################################################################
option(MODULE_BUILD "Build VOTCA modules one-by-one" OFF)
if(MODULE_BUILD)
  message(STATUS "Doing Module build")
  include(${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules/BuildModules.cmake)
  return()
endif()

option(BUILD_OWN_GROMACS "Build our own gromacs" OFF)
if(BUILD_OWN_GROMACS)
  include(${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules/BuildGromacs.cmake)
endif()

if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/.git)
  find_package(Git)
endif(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/.git)

######################################
# Include the following subdirectory # 
######################################
if(BUILD_TOOLS)
  add_subdirectory(tools)
endif()

set(ENABLED_VOTCA_PACKAGES tools)
set(VOTCA_TOOLS_LIBRARY votca_tools CACHE STRING "tools library name")
set(VOTCA_TOOLS_INCLUDE_DIR
  "${CMAKE_CURRENT_SOURCE_DIR}/tools/include;${CMAKE_CURRENT_BINARY_DIR}/tools/include"
  CACHE PATH "tools include path")
set(VOTCA_COMPARE ${CMAKE_BINARY_DIR}/tools/scripts/votca_compare)

if(BUILD_OWN_GROMACS)
  set(VOTCA_PATH "${VOTCA_PATH}:${GROMACS_PATH}")
endif()
set(VOTCA_CSG_DEFAULTS ${CMAKE_BINARY_DIR}/csg/share/xml/csg_defaults.xml)

set(VOTCA_SHARE ${CMAKE_SOURCE_DIR}/csg/share)
set(CSG_INVERSE ${CMAKE_BINARY_DIR}/csg/scripts/csg_inverse)
set(CSG_CALL ${CMAKE_BINARY_DIR}/csg/scripts/csg_call)
set(VOTCA_PATH "${CMAKE_BINARY_DIR}/csg/scripts:${CMAKE_BINARY_DIR}/csg/src/tools")

if(BUILD_CSG)
  add_subdirectory(csg)
  list(APPEND ENABLED_VOTCA_PACKAGES csg)
endif()

set(VOTCA_CSG_LIBRARY votca_csg CACHE STRING "csg library name")
set(VOTCA_CSG_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/csg/include
	CACHE PATH "csg include path")

if(BUILD_CSG_TUTORIALS)
  add_subdirectory(csg-tutorials)
  list(APPEND ENABLED_VOTCA_PACKAGES csg-tutorials)
endif()

if(BUILD_CTP)
  add_subdirectory(ctp)
  list(APPEND ENABLED_VOTCA_PACKAGES ctp)
  set(VOTCA_CTP_LIBRARY votca_ctp CACHE STRING "ctp library name")
  set(VOTCA_CTP_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/ctp/include
	  CACHE PATH "ctp include path")
endif()

#find_package(Doxygen)

add_subdirectory(share/doc)
add_subdirectory(share/format)

include(FeatureSummary)
feature_summary(INCLUDE_QUIET_PACKAGES WHAT ALL)
