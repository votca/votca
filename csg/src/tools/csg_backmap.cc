/*
 * Copyright 2009-2021 The VOTCA Development Team (http://www.votca.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <fstream>
#include <stddef.h>
#include <stdexcept>
#include <string>
#include <votca/csg/cgengine.h>
#include <votca/csg/csgapplication.h>
#include <votca/csg/topology.h>
#include <votca/csg/topologyreader.h>
#include <votca/csg/trajectoryreader.h>
#include <votca/csg/trajectorywriter.h>

using namespace std;
using namespace votca::csg;

class CsgBackmapApp : public CsgApplication {
    public:
    string ProgramName() { return "csg_backmap"; }
    void HelpText(ostream &out) {
        out << "Convert a reference coarse-grained trajectory or configuration into an "
               "atomistic one \n"
            << "based on a mapping xml-file. The mapping can be applied to either "
               "an entire trajectory \n"
            << "or a selected set of frames only (see options).\n"
            << "Examples:\n"
            << "* csg_backmap --no-map --top CG-topol.tpr --trj CG-traj.trr --out Hybrid-traj.trr "
               "--cg-map cg-map.xml --top-atom FA-topol.tpr --trj-atom FA-trj.trr\n";
    }

    bool DoTrajectory() { return true; }
    bool DoMapping() { return true; }

    void Initialize() {
        CsgApplication::Initialize();
        AddProgramOptions()("out", boost::program_options::value<string>(),
                            "  output file for coarse-grained trajectory")(
            "cg-map", boost::program_options::value<string>(), "  cg mapping file for backmapping")(
            "top-atom", boost::program_options::value<string>(), "  atomistic topology containing at least one relevant molecule")(
            "trj-atom", boost::program_options::value<string>(), "  atomistic trajectory containing at least one relevant molecule");
    }

    bool EvaluateOptions() {
        CsgApplication::EvaluateOptions();
        CheckRequired("trj", "no trajectory file specified");
        CheckRequired("out", "need to specify output trajectory");
        CheckRequired("cg-map", "need to specify cg mapping file for backmapping");
        CheckRequired("top-atom", "need to specify atomistic topology containing at least one relevant molecule");
        CheckRequired("trj-atom", "need to specify atomistic trajectory containing at least one relevant molecule");
        return true;
    }

    void BeginEvaluate(Topology *top, Topology *top_ref);
    void EvalConfiguration(Topology *top, Topology *top_ref);

    void EndEvaluate() {
        _writer->Close();
        delete _writer;
    }

    protected:
        TrajectoryWriter *_writer;
        Topology _top_atomistic,_top_backmapped;
        TrajectoryReader *_trjreader_atomistic;
        TopologyReader *_topreader_atomistic;
        TopologyMap *_map_backmapping;
};

void CsgBackmapApp::BeginEvaluate(Topology *top, Topology *top_atom) {
    string out = OptionsMap()["out"].as<string>();
    cout << "writing hybrid trajectory to " << out << endl;
    _writer = TrjWriterFactory().Create(out);
    if (_writer == NULL)
        throw runtime_error("output format not supported: " + out);

    // create reader for atomistic topology
    _topreader_atomistic = TopReaderFactory().Create(_op_vm["top-atom"].as<string>());
    if (_topreader_atomistic == NULL)
        throw runtime_error(string("input format not supported: ") + _op_vm["top-atom"].as<string>());
    _topreader_atomistic->ReadTopology(_op_vm["top-atom"].as<string>(), _top_atomistic);
    cout << "I have " << _top_atomistic.BeadCount() << " beads in " << _top_atomistic.MoleculeCount() << " molecules" << endl;
    _top_atomistic.CheckMoleculeNaming();  
  
    _trjreader_atomistic = TrjReaderFactory().Create(_op_vm["trj-atom"].as<string>());
    if (_trjreader_atomistic == NULL)
        throw runtime_error(string("input format not supported: ") + _op_vm["trj-atom"].as<string>());
    // open the trajectory
    _trjreader_atomistic->Open(_op_vm["trj-atom"].as<string>());
    // read in first frame
    _trjreader_atomistic->FirstFrame(_top_atomistic);

    _writer->Open(out);
};

void CsgBackmapApp::EvalConfiguration(Topology *top, Topology *top_ref) {
    if (top->BeadCount() == 0)
    throw std::runtime_error(
        "CG Topology has 0 beads, check your mapping file!");
    
    // create an extra CG Engine for the backmapping
    CGEngine cg_backmap;
    // read in the coarse graining definitions (xml files)
    cg_backmap.LoadMoleculeType(_op_vm["cg-map"].as<string>());

    _map_backmapping = cg_backmap.CreateAATopology(*top, _top_backmapped, _top_atomistic);
    _map_backmapping->Apply();
    
    // Now combine the backmapped atomistic and the CG topology into one 
    Topology *hybtol = new Topology();

    ResidueContainer::iterator it_res;
    MoleculeContainer::iterator it_mol;

    hybtol->setBox(top->getBox());
    hybtol->setTime(top->getTime());
    hybtol->setStep(top->getStep());

    // for the moment only copy the residues from the backmapped topology and then add the cg beads to them!
    for (it_res = _top_backmapped.Residues().begin(); it_res != _top_backmapped.Residues().end(); ++it_res) {
        hybtol->CreateResidue((*it_res)->getName());
    }    
    /*for (it_res = top->Residues().begin(); it_res != top->Residues().end(); ++it_res) {
        hybtol->CreateResidue((*it_res)->getName());
    }*/

    // copy all molecules and beads
    for (it_mol = _top_backmapped.Molecules().begin(); it_mol != _top_backmapped.Molecules().end(); ++it_mol) {
        Molecule *mi = hybtol->CreateMolecule((*it_mol)->getName());
        for (int i = 0; i < (*it_mol)->BeadCount(); i++) {
            // copy atomistic beads of molecule
            int beadid = (*it_mol)->getBead(i)->getId();
            
            Bead *bi = (*it_mol)->getBead(i);
            if (!hybtol->BeadTypeExist(bi->getType())) {
                hybtol->RegisterBeadType(bi->getType());
            }

            // preliminary, give beads of the backmapped topology the name strapped of the resid and the resname:
            string name = bi->getName();
            string strapped_name;
            // first check, whether the name is of that type, otherwise do not alter the bead name
            if ( (name.find_last_of(":")) != string::npos ){
                std::size_t pos_second_colon = name.find_last_of(":");
                strapped_name = name.substr (pos_second_colon+1);
            }
            else{
                strapped_name = name;
            }
            
            Bead *bn = hybtol->CreateBead(bi->getSymmetry(), strapped_name, bi->getType(), bi->getResnr(), bi->getMass(), bi->getQ());
            bn->setPos(bi->getPos());
            if (bi->HasVel()) bn->setVel(bi->getVel());
            if (bi->HasF()) bn->setF(bi->getF());
            if (bi->HasE()) bn->setE(bi->getE());
            mi->AddBead(hybtol->Beads()[beadid], (*it_mol)->getBeadName(i));
        }

        if (mi->getId() < top->MoleculeCount()) {
            // copy cg beads of molecule
            Molecule *cgmol = top->Molecules()[mi->getId()];
            for (int i = 0; i < cgmol->BeadCount(); i++) {
                Bead *bi = cgmol->getBead(i);
                // todo: this is a bit dirty as a cg bead will always have the resid
                // of the first bead of the backmapped molecule
                Bead *bbackmapped_1 = (*it_mol)->getBead(0);
                Bead *bn = hybtol->CreateBead(bi->getSymmetry(), bi->getName(), bi->getType(), bbackmapped_1->getResnr(), bi->getMass(), bi->getQ());
                bn->setPos(bi->getPos());
                if (bi->HasVel()) bn->setVel(bi->getVel());
                if (bi->HasF()) bn->setF(bi->getF());
                if (bi->HasE()) bn->setE(bi->getE());
                mi->AddBead(bi, bi->getName());
            }
        }
    }
    _writer->Write(hybtol);
};

int main(int argc, char **argv) {
    CsgBackmapApp app;
    return app.Exec(argc, argv);
}