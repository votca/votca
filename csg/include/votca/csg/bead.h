/*
 * Copyright 2009-2019 The VOTCA Development Team (http://www.votca.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef _VOTCA_CSG_BEAD_H
#define _VOTCA_CSG_BEAD_H

#include <assert.h>
#include <cassert>
#include <string>
#include <votca/tools/property.h>
#include <votca/tools/types.h>
#include <votca/tools/vec.h>

#include "basebead.h"

namespace votca {
namespace csg {

class Topology;
class Molecule;

using namespace votca::tools;

/**
 * \brief information about a bead
 *
 * The Bead class describes an atom or a coarse grained bead. It stores
 * information like the id, the name, the mass, the
 * charge and the residue it belongs to. The coordinates are stored in the
 * configuration class.
 *
 * \todo change resnr to pointer
 * \todo make sure bead belongs to topology
 **/
class Bead : public BaseBead {
 public:
  /**
   * destructor
   */
  virtual ~Bead() {}

  /**
   * get the residu number of the bead
   * \return residue id
   */
  const int &getResnr() const { return residue_number_; }

  /**
   * get the mass of the bead
   * \return bead mass
   */
  const double &getM() const {
    std::cerr << "WARNING getM is depricated use getMass" << std::endl;
    return getMass();
  }

  /**
   * set the mass of the bead
   * \param m bead mass
   */
  void setM(const double &m) {
    std::cerr << "WARNING setM is depricated use setMass" << std::endl;
    setMass(m);
  }

  /**
   * get the charge of the bead
   * \return - base bead charge
   */
  virtual const double &getQ() const { return charge_; }

  /**
   * set the charge of the base bead
   * \param[in] - base bead position
   */
  virtual void setQ(const double &q) { charge_ = q; }

  /**
   * \brief get the symmetry of the bead
   *
   * Returns the number of unique axis of the bead, it can be
   * 1 for a spherical bead
   * 3 for an ellipsoidal bead
   * 2 (currently not used), could be disk like particle
   *
   * \return bead symmetry
   */
  byte_t getSymmetry() const { return symmetry_; }

  /**
   * set the velocity of the bead
   * @param r bead velocity
   */
  void setVel(const vec &r);

  /**
   * get the velocity of the bead
   * \return bead velocity
   */
  const vec &getVel() const;

  /**
   * \brief set first orientation (normal vector) vector of bead
   *
   * see getU for details
   *
   * @param u bead orientation u
   */
  void setU(const vec &u);

  /**
   * \brief get first orientation (normal vector) vector of bead
   *
   * Non-spherical beads (symmetry 3) have a internal coordinates system and the
   * axes are denoted as u, v and w. Currently the non-spherical mapping is
   * hardcoded and
   * the axis u is calculated by the eigenvector with the lowest eigenvector of
   * the mapped beads and has the meaning of a normal vector if the reference
   * beads
   * have a disc like shape. The sign of the normal vector is determined in
   * combination
   * with the vectors v and w to build up a right handed (??) coordinate system.
   *
   * \return bead orientation u
   */
  const vec &getU() const;

  /**
   * \brief set second orientation vector of bead
   *
   * see getV for details
   *
   * @param v bead orientation v
   */
  void setV(const vec &v);

  /**
   * \brief get second orientation vector of bead
   *
   * Non-spherical beads (symmetry 3) have a internal coordinates system and the
   * axes are denoted as u, v and w. Currently the non-spherical mapping is
   * hardcoded and
   * the axis v is the vector which connects first and second reference atom
   * in the mapping (only orthogonal component to u).
   *
   * \return bead orientation u
   */
  const vec &getV() const;

  /**
   * \brief set third orientation vector of bead
   *
   * see getW for details
   *
   * @param w bead orientation w
   */
  void setW(const vec &w);

  /**
   * \brief get third orientation vector of bead
   *
   * Non-spherical beads (symmetry 3) have a internal coordinates system and the
   * axes are denoted as u, v and w. Currently the non-spherical mapping is
   * hardcoded and
   * the axis w is orthogonal to u and v.
   *
   * \return bead orientation w
   */
  const vec &getW() const;

  /**
   * direct access (read/write) to the velocity of the bead
   * \return reference to velocity
   */
  vec &Vel() {
    assert(bead_velocity_set_ &&
           "Cannot access velocity, it has not been set.");
    return velocity_;
  }

  /**
   * direct access (read/write) to orientation u of the bead
   * \return reference to u
   */
  vec &U() {
    assert(bU_ && "Cannot access bead orientation u, has not been set.");
    return u_;
  }

  /**
   * direct access (read/write) to the orientation v of the bead
   * \return reference to v
   */
  vec &V() {
    assert(bV_ && "Cannot access bead orientation v, has not been set.");
    return v_;
  }

  /**
   * direct access (read/write) to the orientation w of the bead
   * \return reference to w
   */
  vec &W() {
    assert(bW_ && "Cannot access bead orientation w, has not been set.");
    return w_;
  }

  /**
   * direct access (read/write) to the force of the bead
   * \return reference to force
   */
  vec &F() {
    assert(bead_force_set_ && "Cannot access bead force, has not been set.");
    return bead_force_;
  }
  
  /**
   * direct access (read/write) to the total energies of the bead
   * \return reference to force
   */
  double &E() {
    assert(bead_energy_set_ && "Cannot access bead energy, has not been set.");
    return bead_energy_;
  }  

  /**
   * set force acting on bead
   * @param bead_force force
   */
  void setF(const vec &bead_force);

  /**
   * \brief get the force acting on the bead
   *
   * Forces have to be provided by the trajectory. If beads are mapped, forces
   * of coarse-grained beads are also calculated.
   *
   * \return force on bead
   */
  const vec &getF() const;
  
  /**
   * set total energy of bead
   * @param E energy
   */
  void setE(const double &E);

  /**
   * \brief get the total energy of the bead
   *
   * Energies have to be provided by the trajectory. If beads are mapped, total energies
   * of coarse-grained beads are also calculated.
   *
   * \return total energy of the bead
   */
  const double &getE() const;  
  

  /** does this configuration store velocities? */
  bool HasVel() { return bead_velocity_set_; }

  /** does this configuration store forces? */
  bool HasF() { return bead_force_set_; }
  
  /** does this configuration store total energies? */
  bool HasE() { return bead_energy_set_; }  

  /** does this configuration store u-orientations? */
  bool HasU() { return bU_; }

  /** does this configuration store v-orientations? */
  bool HasV() { return bV_; }

  /** does this configuration store w-orientations? */
  bool HasW() { return bW_; }

  /** dos the bead store a velocity */
  void HasVel(bool b);

  /** dos the bead store a force */
  void HasF(bool b);

  /** dos the bead store a total energy */
  void HasE(bool b);  
  
  /** doe the bead store an orientation u */
  void HasU(bool b);

  /** doe the bead store an orientation v */
  void HasV(bool b);

  /** doe the bead store an orientation w */
  void HasW(bool b);
  
  /**
   * \returns add descriptor (used for ML)
   */
  void addDescriptor(const vec &des1, const vec &des2, const vec &des3, const int &num);
  
  /**
   * sets descriptor size to zero
   */
  void clearDescriptors() { _descriptors1.clear(); _descriptors2.clear(); _descriptors3.clear(); _descriptornumber.clear(); };  
  
  /**
   * \returns number of descriptors
   */
  unsigned int DescriptorsSize() { return _descriptors1.size(); };
  
  /**
   * \returns ith descriptor1
   */
  const vec &getDescriptor1(int i) const;

  /**
   * \returns ith descriptor2
   */
  const vec &getDescriptor2(int i) const;

  /**
   * \returns ith descriptor3
   */
  const vec &getDescriptor3(int i) const;
  
  /**
   * \returns ith descriptornumber
   */
  const int &getDescriptorNumber(int i) const;
  
  /**
   * If it is a mapped beads, returns te bead id the cg bead was created from
   * \return vector of bead ids of reference atoms
   */
  const std::vector<int> &ParentBeads() { return parent_beads_; };
  
  /**
   * \brief Clears out all parent beads
   **/
  void ClearParentBeads() { parent_beads_.clear(); }

  /**
   * \brief Adds the id of a parent bead
   **/
  void AddParentBead(int parent_bead_id) {
    parent_beads_.push_back(parent_bead_id);
  }

 protected:
  std::vector<int> parent_beads_;

  vector<vec>  _descriptors1;
  vector<vec>  _descriptors2;
  vector<vec>  _descriptors3;
  vector<int>  _descriptornumber;

  // TODO: this is so far a pointer. this should change! each bead should have
  // own options.
  // Property *options_;

  byte_t symmetry_;
  double charge_;

  int residue_number_;

  vec velocity_, bead_force_, u_, v_, w_;
  double bead_energy_;

  bool bead_velocity_set_;
  bool bU_;
  bool bV_;
  bool bW_;
  bool bead_force_set_;
  bool bead_energy_set_;  

  /// constructor
  Bead(Topology *owner, int id, std::string type, byte_t symmetry,
       std::string name, int resnr, double m, double q)
      : symmetry_(symmetry), charge_(q), residue_number_(resnr) {
    topology_item_._parent = owner;
    setId(id);
    setType(type);
    setName(name);
    setMass(m);
    bead_position_set_ = false;
    bead_velocity_set_ = false;
    bU_ = false;
    bV_ = false;
    bW_ = false;
    bead_force_set_ = false;
    bead_energy_set_ = false;
    _descriptors1.clear();
    _descriptors2.clear();
    _descriptors3.clear();
    _descriptornumber.clear();
  }

  // void *_userdata;

  friend class Topology;
  friend class Molecule;
};

inline void Bead::setVel(const vec &r) {
  bead_velocity_set_ = true;
  velocity_ = r;
}

inline const vec &Bead::getVel() const {
  assert(bead_velocity_set_ &&
         "Cannot access bead velocity, has not been set.");
  return velocity_;
}

inline void Bead::setU(const vec &u) {
  bU_ = true;
  u_ = u;
}

inline const vec &Bead::getU() const {
  assert(bU_ && "Cannot access bead orientation u, has not been set.");
  return u_;
}

inline void Bead::setV(const vec &v) {
  bV_ = true;
  v_ = v;
}

inline const vec &Bead::getV() const {
  assert(bV_);
  return v_;
}

inline void Bead::setW(const vec &w) {
  bW_ = true;
  w_ = w;
}

inline const vec &Bead::getW() const {
  assert(bW_ && "Cannot access bead orientation w, has not been set.");
  return w_;
}

inline void Bead::setF(const vec &bead_force) {
  bead_force_set_ = true;
  bead_force_ = bead_force;
}

inline const vec &Bead::getF() const {
  assert(bead_force_set_ && "Cannot access bead force, has not been set.");
  return bead_force_;
}

inline void Bead::setE(const double &bead_energy) {
  bead_energy_set_ = true;
  bead_energy_ = bead_energy;
}

inline const double &Bead::getE() const {
  assert(bead_energy_set_ && "Cannot access bead energy, has not been set.");
  return bead_energy_;
}

inline void Bead::addDescriptor(const vec &des1, const vec &des2, const vec &des3, const int &num) { 
    _descriptors1.push_back(des1);
    _descriptors2.push_back(des2);
    _descriptors3.push_back(des3);
    _descriptornumber.push_back(num);
}

inline const vec &Bead::getDescriptor1(int i) const { 
    return _descriptors1[i];
}

inline const vec &Bead::getDescriptor2(int i) const { 
    return _descriptors2[i];
}

inline const vec &Bead::getDescriptor3(int i) const { 
    return _descriptors3[i];
}

inline const int &Bead::getDescriptorNumber(int i) const {
    return _descriptornumber[i];
}

inline void Bead::HasVel(bool b) { bead_velocity_set_ = b; }

inline void Bead::HasF(bool b) { bead_force_set_ = b; }

inline void Bead::HasE(bool b) { bead_energy_set_ = b; }

inline void Bead::HasU(bool b) { bU_ = b; }

inline void Bead::HasV(bool b) { bV_ = b; }

inline void Bead::HasW(bool b) { bW_ = b; }
}  // namespace csg
}  // namespace votca

#endif  // _VOTCA_CSG_BEAD_H
