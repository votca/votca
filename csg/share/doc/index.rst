.. _csg:

Coarsegraining
==========

.. toctree::
   :maxdepth: 1

   introduction
   theory
   input_files
   preparing
   methods
   advanced_topics
   reference
   bibliography


Citations
---------

Development of this software depends on academic research grants. If
you are using the package, please cite the following papers:

- [scherer_ML_2020]_ Kernel-based machine learning in VOTCA
- [scherer_understanding_2018]_ Force-matching with three-body interactions in VOTCA
- [mashayakrelative]_ Relative entropy and optimization-driven coarse-graining methods in VOTCA
- [ruhle2011hybrid]_ Hybrid approaches to coarse-graining using the VOTCA package: liquid hexane
- [Ruehle:2009.a]_ Versatile Object-oriented Toolkit for Coarse-graining Applications


Copyright
---------

VOTCA is free software. The entire package is available under the Apache
License. For details, check the LICENSE file in the source code. 
