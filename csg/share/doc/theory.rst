Theoretical background
======================

.. _theory_mapping:

Mapping
-------

The mapping is an operator that establishes a link between the atomistic
and coarse-grained representations of the system. An atomistic system is
described by specifying the values of the Cartesian coordinates and
momenta

.. math::

   \begin{aligned}
   \mathbf r^n &= \{\mathbf r_1,\dots,\mathbf r_n\}, \\
   \mathbf p^n &= \{\mathbf p_1,\dots,\mathbf p_n\}.
   \end{aligned}

of the :math:`n` atoms in the system. [#f1]_ On a coarse-grained level,
the coordinates and momenta are specified by the positions and momenta
of CG sites

.. math::

   \begin{aligned}
   \mathbf R^N = \{\mathbf R_1,\dots,\mathbf R_N\}, \\
   \mathbf P^N = \{\mathbf P_1,\dots,\mathbf P_N\}.
   \end{aligned}

Note that capitalized symbols are used for the CG sites while lower
case letters are used for the atomistic system.

The mapping operator :math:`{\mathbf c}_I` is defined by a matrix for each
bead :math:`I` and links the two descriptions

.. _theory_eq_mapping_scheme:

.. math::

   \begin{aligned}
    {\mathbf R}_I &= \sum_{i=1}^{n}c_{Ii}\mathbf r_i, \\
    {\mathbf P}_I &=
       M_I \dot{{\mathbf R}}_I =
       M_I \sum_{i=1}^{n}c_{Ii} \dot{{\mathbf r}}_i =
       M_I \sum_{i=1}^{n} \frac{ c_{Ii}} {m_i} {\mathbf p}_i .
   \end{aligned}

for all :math:`I = 1,\dots,N`.

If an atomistic system is translated by a constant vector, the
corresponding coarse-grained system is also translated by the same
vector. This implies that, for all :math:`I`,

.. math:: \sum_{i=1}^{n}c_{Ii}=1.

In some cases it is useful to define the CG mapping in such a way that
certain atoms belong to several CG beads at the same
time [Fritz:2009]_. Following
ref. [Noid:2008.1]_, we define two sets of atoms for
each of the :math:`N` CG beads. For each site :math:`I`, a set of
*involved* atoms is defined as

.. math:: {\cal I}_I=\{i|c_{Ii}\ne0\}.

An atom :math:`i` in the atomistic model is involved in a CG site, *I*,
if and only if this atom provides a nonzero contribution to the sum in
:ref:`the equation above<theory_eq_mapping_scheme>`.

A set of *specific* atoms is defined as

.. math:: {\cal S}_I=\{i|c_{Ii}\ne0 \text{ and } c_{Ji}=0 \text{ for all } J \ne I\}.

In other words, atom :math:`i` is specific to site :math:`I` if and
only if this atom is involved in site :math:`I` and is not involved in
the definition of any other site.

The CG model will generate an equilibrium distribution of momenta that
is consistent with an underlying atomistic model if all the atoms are
*specific* and if the mass of the :math:`I^\text{th}` CG site is given
by [Noid:2008.1]_

.. math::

   M_I= \left( \sum_{i \in {\cal I}_I}\frac{c_{Ii}^2}{m_i} \right)^{-1}.

If all atoms are specific and the center of mass of a bead is used for
mapping, then :math:`c_{Ii} = \frac{m_i}{M_I}`, and the
condition is automatically satisfied.

.. rubric:: Footnotes
.. [#] In what follows we adopt notations of ref. [Noid:2008.1]_.



Boltzmann inversion
-------------------

Boltzmann inversion is mostly used for *bonded* potentials, such as
bonds, angles, and torsions [Tschoep:1998]_. Boltzmann
inversion is structure-based and only requires positions of atoms.

The idea of Boltzmann inversion stems from the fact that in a canonical
ensemble *independent* degrees of freedom :math:`q` obey the Boltzmann
distribution, i. e.



.. math::

   P(q) = Z^{-1} \exp\left[ - \beta U(q) \right]~,
   \label{eq:boltzmann}

where is a partition function, . Once :math:`P(q)` is known, one can
obtain the coarse-grained potential, which in this case is a potential
of mean force, by inverting the probability distribution :math:`P(q)` of
a variable :math:`q`, which is either a bond length, bond angle, or
torsion angle

.. math::

   U(q) = - k_\text{B} T \ln  P(q) ~.
   \label{eq:inv_boltzmann}

The normalization factor :math:`Z` is not important since it would only
enter the coarse-grained potential :math:`U(q)` as an irrelevant
additive constant.

Note that the histograms for the bonds :math:`H_r(r)`, angles
:math:`H_\theta(\theta)`, and torsion angles :math:`H_\varphi(\varphi)`
have to be rescaled in order to obtain the volume normalized
distribution functions :math:`P_r(r)`, :math:`P_\theta(\theta)`, and
:math:`P_\varphi(\varphi)`, respectively,

.. _theory_eq_boltzmann_norm:

.. math::

   \begin{aligned}
   P_r(r) = \frac{H_r(r)}{4\pi r^2}~,\;
   P_\theta(\theta) = \frac{H_\theta(\theta)}{\sin \theta}~,\;
   P_\varphi(\varphi) = H_\varphi (\varphi)~,
   \end{aligned}

where :math:`r` is the bond length :math:`r`, :math:`\theta` is the
bond angle, and :math:`\varphi` is the torsion angle. The bonded
coarse-grained potential can then be written as a sum of distribution
functions

.. _theory_eq_boltzmann_pmf:

.. math::

   \begin{aligned}
       U({r}, \theta, \varphi) &= U_r({r}) + U_{\theta}(\theta) + U_{\varphi}(\varphi)~, \\
       U_q({q}) &= - k_\text{B} T \ln P_q( q ),\; q=r, \theta, \varphi~.
       \nonumber\end{aligned}

On the technical side, the implementation of the Boltzmann inversion
method requires *smoothing* of :math:`U(q)` to provide a continuous
force. Splines can be used for this purpose. Poorly and unsampled
regions, that is regions with high :math:`U(q)`, shall be
*extrapolated*. Since the contribution of these regions to the canonical
density of states is small, the exact shape of the extrapolation is less
important.

Another crucial issue is the cross-correlation of the coarse-grained
degrees of freedom. Independence of the coarse-grained degrees of
freedom is the main assumption that allows factorization of the
probability distribution and the potential :ref:`as in the above equation<theory_eq_boltzmann_pmf>`.
Hence, one has to carefully check whether this assumption holds in
practice. This can be done by performing coarse-grained simulations and
comparing cross-correlations for all pairs of degrees of freedom in
atomistic and coarse-grained resolution, e. g. using a two-dimensional
histogram, analogous to a Ramachandran plot.  [#f2]_

.. rubric:: Footnotes
.. [#] Checking the linear correlation coefficient does not guarantee
   statistical independence of variables, for example
   :math:`c(x, x^2)=0` if :math:`x` has a symmetric probability density
   :math:`P(x) = P(-x)`. This case is often encountered in systems used
   for coarse-graining.

Separation of bonded and non-bonded degrees of freedom
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When coarse-graining polymeric systems, it is convenient to treat bonded
and non-bonded interactions separately [Tschoep:1998]_.
In this case, sampling of the atomistic system shall be performed on a
special system where non-bonded interactions are artificially removed,
so that the non-bonded interactions in the reference system do not
contribute to the bonded interactions of the coarse-grained model.

This can be done by employing exclusion lists using with the option
``--excl``. This is described in detail in :ref:`methods_exclusions`.

.. figure:: fig/excl.png
   :align: center

   Example of excluded interactions.

.. _theory_iterative_methods:

Iterative methods
-----------------

.. _theory_fig_iterative_simple:

.. figure:: fig/iteration-scheme-simple.png
   :align: center

   Block-scheme of an iterative method.

Iterative workflow control is essential for the IBI and IMC methods. The
general idea of iterative workflow is sketched in
:ref:`the block-scheme above<theory_fig_iterative_simple>`. A run starts with
an initial guess during the global initialization phase. This guess is used for
the first sampling step, followed by an update of the potential. The update
itself often requires additional postprocessing such as smoothing,
interpolation, extrapolation or fitting. Different methods are available to
update the potential, for instance Iterative Boltzmann Inversion (see
:ref:`theory_iterative_boltzmann_inversion`) or Inverse Monte Carlo (see
:ref:`theory_inverse_monte_carlo`). The whole procedure is then iterated until
a convergence criterion is satisfied.

.. _theory_iterative_boltzmann_inversion:

Iterative Boltzmann Inversion
-----------------------------

Iterative Boltzmann inversion (IBI) is a natural extension of the
Boltzmann inversion method. Since the goal of the coarse-grained model
is to reproduce the distribution functions of the reference system as
accurately as possible, one can also iteratively refine the
coarse-grained potentials using some numerical scheme.

In IBIthe potential update :math:`\Delta U` is given
by [Reith:2003]_

.. math::

    \begin{aligned}
    U^{(n+1)} &= U^{(n)} + \lambda \Delta U^{(n)}~, \\
    \Delta U^{(n)} &=  k_\text{B} T \ln  \frac{P^{(n)}}{P_{\rm ref}}
    =  U_\text{PMF}^\text{ref} - U_\text{PMF}^{(n)}~.
    \end{aligned}
    \label{eq:iter_boltzmann}

Here :math:`\lambda \in (0,1]` is a numerical factor which helps to
stabilize the scheme.

The convergence is reached as soon as the distribution function
:math:`P^{(n)}` matches the reference distribution function
:math:`P_{\rm ref}`, or, in other words, the potential of mean force,
:math:`U_\text{PMF}^{(n)}`, converges to the reference potential of mean
force.

IBIcan be used to refine both bonded and non-bonded potentials. It is
primarily used for simple fluids with the aim to reproduce the radial
distribution function of the reference system in order to obtain
non-bonded interactions. On the implementation side, IBIhas the same
issues as the inverse Boltzmann method, i. e. smoothing and
extrapolation of the potential must be used.

.. _theory_inverse_monte_carlo:

Inverse Monte Carlo
-------------------

Inverse Monte Carlo (IMC) is an iterative scheme which additionally
includes cross correlations of distributions. A detailed derivation of
the IMCmethod can be found in ref. [Lyubartsev:1995]_.

The potential update :math:`\Delta U` of the IMCmethod is calculated by
solving a set of linear equations

.. math::

   \begin{aligned}
       \left<S_{\alpha}\right> - S_{\alpha}^{\text{ref}}= A_{\alpha \gamma} \Delta U_{\gamma}~,
     \label{eq:imc}\end{aligned}

where

.. _theory_eq_covariance:

.. math::

   \begin{aligned}
     A_{\alpha \gamma} = \frac{\partial \left< S_{\alpha} \right> }{\partial U_{\gamma}}  =
     \beta \left( \left<S_{\alpha} \right>\left<S_{\gamma} \right> - \left<S_{\alpha} S_{\gamma} \right>  \right)~,
     \nonumber\end{aligned}

and :math:`S` the histogram of a coarse-grained variable of interest.
For example, in case of coarse-graining of the non-bonded interactions
which depend only on the distance :math:`r_{ij}` between particles
:math:`i` and :math:`j` and assuming that the interaction potential is
short-ranged, i.e. :math:`U(r_{ij})=0` if
:math:`r_{ij} \ge r_{\text{cut} }`, the average value of
:math:`S_{\alpha}` is related to the radial distribution function
:math:`g(r_{\alpha})` by

.. math::

   \left< S_{\alpha} \right> =  \frac{N(N-1)}{2} \frac{4 \pi r_{\alpha}^2 \Delta r} {V}g(r_{\alpha})~,
     \label{eq:s_mean}

where :math:`N` is the number of atoms in the system
(:math:`\frac{1}{2} N(N-1)` is then the number of all pairs),
:math:`\Delta r` is the grid spacing, :math:`r_{\text{cut}}/M`,
:math:`V` is the total volume of the system. In other words, in this
particular case the physical meaning of :math:`S_{\alpha}` is the number
of particle pairs with interparticle distances
:math:`r_{ij} = r_{\alpha}` which correspond to the tabulated value of
the potential :math:`U_{\alpha}`.

.. _theory_inverse_monte_carlo_regularization:

Regularization of Inverse Monte Carlo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get a well defined cross correlation matrix,
:math:`A_{\alpha \gamma}`, enough sampling is needed. If there is not
enough smapling or the initial potential guess is far from the real
solution of the inverse problem, the algorithm might not converge to a
stable solution. To overcome this instability problem one could
reformulate :ref:`the above equation<theory_eq_covariance>` by addition of a penalty term. In
this case the potential update is computed as
follows:[Murtola:2007]_

.. math::

   \Delta U_\gamma = \arg \min \| A_{\alpha \gamma} \Delta U_\gamma - \left(\left<S_{\alpha}\right> - S_{\alpha}^{\text{ref}}\right) \|^2 + \lambda \| R \Delta U_{\gamma} \|^{2}

This equation is known as Tikhonov regularization, where
:math:`R` is the regularization operator, which here is the identity
matrix and :math:`\lambda >0 ` is the regularization parameter. The
optimal choice for :math:`\lambda` can only be determined if the exact
solution of the inverse problem is known, which in practice is not the
case. To get a good initial guess on the magnitude of the regularization
parameter a singular value decomposition of the matrix
:math:`A_{\alpha \gamma}` might help. A good :math:`\lambda` parameter
should dominate the smallest singular values (squared) but is itself
small compared to the larger ones. [Rosenberger:2016]_


Force Matching
--------------

Force matching (FM) is another approach to evaluate corse-grained
potentials [Ercolessi:1994,Izvekov:2005,Noid:2007]_. In
contrast to the structure-based approaches, its aim is not to reproduce
various distribution functions, but instead to match the multibody
potential of mean force as close as possible with a given set of
coarse-grained interactions.

The method works as follows. We first assume that the coarse-grained
force-field (and hence the forces) depends on :math:`M` parameters
:math:`g_1,...,g_M`. These parameters can be prefactors of analytical
functions, tabulated values of the interaction potentials, or
coefficients of splines used to describe these potentials.

In order to determine these parameters, the reference forces on
coarse-grained beads are calculated by summing up the forces on the
atoms

.. math::

   {{{{\mathbf F}}}}_I^\text{ref} = \sum_{j \in {\cal S_I}} \frac{d_{Ii}}{c_{Ii}} {{{{\mathbf f}}}}_j({{{{\mathbf r}}}^n}),
     \label{eq:force_mapping}

where the sum is over all atoms of the CG site *I* (see
:ref:`theory_mapping`). The :math:`d_{Ij}` coefficients can, in
principle, be chosen arbitrarily, provided that the condition
:math:` \sum_{i=1}^{n}d_{Ii}=1` is
satisfied [Noid:2008.1]_. If mapping coefficients for
the forces are not provided, it is assumed that :math:`d_{Ij} = c_{Ij}`
(see also :ref:`input_files`).

By calculating the reference forces for :math:`L` snapshots we can write
down :math:`N \times L` equations

.. _theory_eq_fmatch1:

.. math::

   {{{{\mathbf F}}}}_{Il}^\text{cg}(g_1, \dots ,g_M)={{{\mathbf F}}}_{il}^\text{ref},\;
     I=1,\dots,N,\; l=1,\dots,L~.

Here :math:`{{{{\mathbf F}}}}_{Il}^\text{ref}` is the force on
the bead :math:`I` and :math:`{{{{\mathbf F}}}}_{Il}^\text{cg} `
is the coarse-grained representation of this force. The index :math:`l`
enumerates snapshots picked for coarse-graining. By running the
simulations long enough one can always ensure that
:math:`M < N \times L`. In this case the set of equations
is overdetermined and can be solved in a least-squares manner.

:math:`{\mathbf F}_{il}^\text{cg}` is, in principle, a non-linear function
of its parameters :math:`\{g_i\}`. Therefore, it is useful to represent
the coarse-grained force-field in such a way that
:ref:`equations <theory_eq_fmatch1>` become linear functions of :math:`{g_i}`.
This can be done using splines to describe the functional form of the
forces [Izvekov:2005]_. Implementation details are
discussed in ref. [Ruehle:2009.a]_.

Note that an adequate sampling of the system requires a large number of
snapshots :math:`L`. Hence, the applicability of the method is often
constrained by the amount of memory available. To remedy the situation,
one can split the trajectory into blocks, find the coarse-grained
potential for each block and then perform averaging over all blocks.

.. _theory_fm_threebody:

Three-body Stillinger-Weber interactions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In addition to non-bonded pair interactions, it is also possible to parametrize non-bonded 
three-body interactions of the Stillinger Weber type [stillinger_computer_1985]_:

.. _theory_eq_SW1:

.. math::

   U = \sum_{I,J\neq I,K>J}f^{\left(3b\right)}\left(\theta_{IJK}\right)\,\exp{\left(\frac{\gamma_{IJ}\sigma_{IJ}}{r_{IJ}-a_{IJ}\sigma_{IJ}} + \frac{\gamma_{IK}\sigma_{IK}}{r_{IK}-a_{IK}\sigma_{IK}}\right)},
  
where :math:`I` is the index of the central bead of a triplet of a CG sites, and :math:`J` 
and :math:`K` are the other two bead indices of a triplet of beads with an angular interaction 
term :math:`f^{\left(3b\right)}\left(\theta_{IJK}\right)` and two exponential terms, 
:math:`\exp\left(\frac{\gamma\sigma}{r-a\sigma}\right)`, which guarantee a smooth switching on of the 
three-body forces at the cutoff distance. We do not limit ourselves to an analytic expression of 
:math:`f^{\left(3b\right)}\left(\theta_{IJK}\right)` as in the original SW potential, but allow for a flexible 
angular dependence of :math:`f^{\left(3b\right)}\left(\theta_{IJK}\right)`. This is done by using a cubic spline 
representation of :math:`f^{\left(3b\right)}\left(\theta\right)`. Treating the two exponential terms as 
prefactors yields a linear dependence of the potential and thus the CG force :math:`{\mathbf F}_{Il}^\text{cg}` on 
the :math:`M` spline coefficients. Therefore, the remaining parameters :math:`a_{IJ}`, :math:`a_{IK}` 
(three-body short-range cutoff), :math:`\sigma_{IJ}`, :math:`\sigma_{IK}` 
(additional parameter, can be set to 1), :math:`\gamma_{IJ}`, and :math:`\gamma_{IK}` (steepness of switching), 
have to be set before solving the set of
:ref:`linear equations <theory_eq_fmatch1>`. For a more detailed description, 
we refer to ref. [scherer_understanding_2018]_.


Relative Entropy
----------------

Relative entropy is a method which quantifies the extent of the
configurational phase-space overlap between two molecular
ensembles [Wu2005]_. It can be used as a measure of the
discrepancies between various properties of the CG system’s and the
target all-atom (AA) ensemble. It has been shown by Shell
S. [Shell2008]_ that one can minimize the relative
entropy metric between the model CG system and the target AA system to
optimize CG potential parameters such that the CG ensemble would mimic
the target AA ensemble.

Relative entropy, :math:`S_{\text{rel}}`, is defined as
[Shell2008]_

.. _theory_eq_srel:

.. math::

   S_{\text{rel}} = \sum_{i}p_{\text{AA}}(r_i) \ln\left(
     \frac{p_{\text{AA}}(r_i)}{p_{\text{CG}}\left(M(r_i)\right)}\right) +
   \langle S_{\text{map}} \rangle_{\text{AA}},

where the sum is over all the configurations of the reference AA
system, :math:`r=\{r_i\} (i=1,2,...)`, :math:`M` is the mapping
operation to generate a corresponding CG configuration, :math:`R_I`,
from a AA configuration, :math:`r_i`, i.e., :math:`R_I = M(r_i)`,
:math:`p_\text{AA}` and :math:`p_\text{CG}` are the configurational
probabilities based on the AA and CG potentials, respectively, and
:math:`\langle S_{\text{map}}\rangle_{\text{AA}}` is the mapping entropy due to the
average degeneracy of AA configurations mapping to the same CG
configuration, given by

.. math::

   \label{eq:smap}
   S_{\text{map}}(R_I)=\ln\sum_{i}\delta_{R_I,M(r_i)} ,

where :math:`\delta` is the Kronecker delta function. Physically,
:math:`S_{\text{rel}}` can be interpreted as the likelihood that one
test configuration of the model CG ensemble is representative of the
target AA ensemble, and when the likelihood is a maximum,
:math:`S_{\text{rel}}` is at a minimum. Hence, the numerical
minimization of :math:`S_{\text{rel}}` with respect to the parameters of
the CG model can be used to optimize the CG model.

In a canonical ensemble, substituting canonical configurational
probabilities into :ref:`the definition for the relative entropy<theory_eq_srel>`,
it simplifies to

.. _theory_eq_srelcan:

.. math::

   S_{\text{rel}}=\beta\langle U_{\text{CG}} - U_{\text{AA}}\rangle_{\text{AA}}
   - \beta\left( A_{\text{CG}} - A_{\text{AA}}\right)
   + \langle S_{\text{map}}\rangle_{\text{AA}} ,

where :math:`\beta={1}/{k_{\text{B}}T}`, :math:`k_{\text{B}}` is the
Boltzmann constant, :math:`T` is the temperature, :math:`U_\text{CG}`
and :math:`U_\text{AA}` are the total potential energies from the CG and
AA potentials, respectively, :math:`A_\text{CG}` and :math:`A_\text{AA}`
are the configurational part of the Helmholtz free energies from the CG
and AA potentials, respectively, and all the averages are computed in
the reference AA ensemble.

Consider a model CG system defined by the CG potentials between various
CG sites such that the CG potentials depend on the parameters
:math:`\boldsymbol\lambda=\{\lambda_1,\lambda_2,...\lambda_n\}`. Then
:math:`\boldsymbol\lambda` are optimized by the relative entropy
minimization. We use the Newton-Raphson strategy for the relative
entropy minimization described in
ref. [Chaimovich2011]_. In this strategy, the CG
potential parameters, :math:`\boldsymbol\lambda`, are refined
iteratively as

.. math::

   \label{eq:newtraph}
   \boldsymbol{\lambda} ^{k+1} = \boldsymbol{\lambda} ^{k} -
   \chi \mathbf{H} ^{-1}\cdot
   \nabla_{\lambda} S_{\text{rel}} ,

where :math:`k` is the iteration index, :math:`\chi\in(0...1)` is the
scaling parameter that can be adjusted to ensure convergence,
:math:`\nabla_{\lambda}S_{\text{rel}}` is the vector of the first
derivatives of :math:`S_{\text{rel}}` with respect to
:math:`boldsymbollambda`, which can be computed from `the above equation<theory_eq_srelcan>`
as

.. math::

   \nabla_{\lambda}S_{\text{rel}} = \beta \left\langle \frac{\partial
     U_{\text{CG}}}{\partial\lambda}\right\rangle_{\text{AA}} - \beta\left\langle
   \frac{\partial U_{\text{CG}}}{\partial\lambda}\right\rangle_{\text{CG}} ,

and :math:`\mathbf{H}` is the Hessian matrix of :math:`S_{\text{rel}}`
given by

.. math::

   \begin{aligned}
   \mathbf{H}_{ij}&=&\beta \left\langle \frac{\partial^2
     U_{\text{CG}}}{\partial\lambda_i\partial\lambda_j}\right \rangle_{\text{AA}} -
   \beta \left\langle \frac{\partial^2
     U_{\text{CG}}}{\partial\lambda_i\partial\lambda_j}\right \rangle_{\text{CG}}
   \nonumber\\ &&+ \beta^2 \left\langle \frac{\partial
     U_{\text{CG}}}{\partial\lambda_i} \frac{\partial
     U_{\text{CG}}}{\partial\lambda_j}\right\rangle_{\text{CG}} \nonumber\\ &&-
   \beta^2 \left\langle \frac{\partial
     U_{\text{CG}}}{\partial\lambda_i}\right\rangle_{\text{CG}} \left\langle
   \frac{\partial U_{\text{CG}}}{\partial\lambda_j}\right\rangle_{\text{CG}}.\end{aligned}

To compute :math:`\nabla_{\lambda}S_{\text{rel}}` and :math:`\mathbf{H}`
from those two equations, we need average CG energy
derivatives in the AA and CG ensembles. For two-body CG pair potentials,
:math:`u_{\text{CG}}`, between CG sites, the ensemble averages of the CG
energy derivatives can be computed as

.. math::

   \begin{aligned}
   \left\langle\left(\frac{\partial^a U_{\text{CG}}}{\partial \lambda^a}\right)^b
   \right\rangle_{\text{AA}}& =
   &\left\langle\left(\sum_{i<j}\frac{\partial^{a}u_{\text{CG}}(r_{ij})}
   {\partial \lambda^a}\right)^b\right\rangle_{\text{AA}}\nonumber \\
   \left\langle\left(\frac{\partial^a U_{\text{CG}}}{\partial \lambda^a}\right)^b
   \right\rangle_{\text{CG}}& =
   &\left\langle\left(\sum_{i<j}\frac{\partial^{a}u_{\text{CG}}(r_{ij})}
   {\partial \lambda^a}\right)^b\right\rangle_{\text{CG}}  ,\end{aligned}

where the sum is performed over all the CG site pairs :math:`(i,j)`,
:math:`a` stands for the 1\ :math:`^{\text{st}}`,
2\ :math:`^{\text{nd}}`,... derivatives and :math:`b` stands for the
different powers, i.e., :math:`b=1,2,...`. For the averages in the AA
ensemble, first a single AA system simulation can be performed and RDFs
between the CG sites in the AA ensemble can be saved, then the average
CG energy derivatives in AA ensemble can be computed by processing the
CG RDFs in the AA ensemble using the CG potentials at each iteration.
For the averages in the CG ensemble, since the CG ensemble changes with
the CG parameters, :math:`\boldsymbol\lambda`, a short CG simulation is
performed at each iteration to generate corresponding CG configurations.

Comparisons between relative entropy and other coarse-graining methods
are made in ref. [rudzinski_coarse-graining_2011]_
and [Chaimovich2011]_. Chaimovich and
Shell [Chaimovich2011]_ have shown that for certain CG
models relative entropy minimization produces the same CG potentials as
other methods, e.g., it is equivalent to the IBI when CG interactions
are modeled using finely tabulated pair additive potentials, and to the
FM when a CG model is based on :math:`N-`\ body interactions, where
:math:`N` is the number of degrees of freedom in the CG model. However,
there are some advantages of using relative entropy based
coarse-graining. Relative entropy method allows to use analytical
function forms for CG potentials, which are desired in theoretical
treatments, such as parametric study of CG potentials, whereas, methods,
like IBI, use tabulated potentials. Recently Lyubartsev et.
al [lyubartsev2010systematic]_ have shows how to use
IMC with an analytical function form, too. BI, IBI, and IMC methods are
based on pair correlations and hence, they are only useful to optimize
2-body CG potentials, whereas, relative entropy uses more generic metric
which offers more flexibility in modeling CG interactions and not only
2-body, but also 3-body (for example see
ref. [lu_coarse-graining_2014]_) and N-body CG
potentials can be optimized. In addition to the CG potential
optimization, the relative entropy metric can also be used to optimize
an AA to CG mapping operator.


Kernel-Based Machine Learning
-----------------------------

Kernel-based machine learning (ML) is a general fitting method that does not explicitly rely on
force field functions. In this context, kernel-based ML is used to learn (coarse-grained) 
two-body, as well as three-body forces as 
a generalized force-matching scheme without a priori fixing the functional 
form of the force functions. A more detailed description
can be found in ref. [scherer_ML_2020]_.

Kernel-based regression for force fields
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, a short recapitulation of the terminology of a Gaussian process 
for physical quantities relevant to liquids:
A sample :math:`i` represents particle :math:`i` and its environment. 
A particle can be an atom or a CG bead. 
Then, a mapping :math:`{\mathbf Q} \mapsto O` between the representation of sample :math:`i`, 
:math:`{\mathbf Q}_i`, and an observable, :math:`O_i = O({\mathbf Q}_i)` is learned.
Here, the observable is a force. Training a kernel model is then 
equivalent to solving the set of linear 
equations :math:`{\mathbf O} = \hat{K} {\mathbf \alpha}`, where the kernel function 
:math:`\hat{K}_{ij} = K({\mathbf Q}_i, {\mathbf Q}_j) = \text{Cov}(O_i, O_j)`
measures the similarity between samples :math:`{\mathbf Q}_i` and :math:`{\mathbf Q}_j`, 
and :math:`{\mathbf \alpha}` is the vector of weight coefficients. The latter is
optimized by inverting the regularized problem

.. _theory_eq_inversion:

.. math::

   {\mathbf \alpha} = \left(\hat{K}+\lambda \mathbb{1}\right)^{-1} {\mathbf O}, 

where :math:`\lambda` implements Tikhonov regularization. Prediction of the
observable for a configuration :math:`{\mathbf Q}^*` is then given by an
expansion of the kernel evaluations on the training set

.. math::

   O({\mathbf Q}^*) = \sum_{i=1}^N \alpha_i K({\mathbf Q}_i, {\mathbf Q}^*),

summing over the :math:`N` training points.

Particle-decomposition ansatz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The above-mentioned formulation can, in principle, be
applied to learn local forces :math:`{\mathbf F}`. Reference simulations 
provide the force of a particle embedded in its environment :math:`{\mathbf Q}`. 
First, the size of particle environments is limited
by setting a cutoff, :math:`r_{\text{cut}}`. In other words, for a 
sample :math:`i` only the neighbors :math:`j` with :math:`r_{ij} \le r_{\text{cut}}`
are added to the environment :math:`{\mathbf Q}_i`.
Then, the environments are further decomposed in 2-body and 3-body terms, 
:math:`{\mathbf q}^{(2)}` and :math:`{\mathbf q}^{(3)}` which are associated to a set of local forces,
:math:`{\mathbf f}^{(2)}` and :math:`{\mathbf f}^{(3)}`. This particle-decomposition 
ansatz assumes that the force acting on a particle due to its environment can be approximated by a
set of constitutive interparticle interactions

.. _theory_eq_decomp1:

.. math::

   {\mathbf F}({\mathbf Q}_i) = \sum_{p = 1}^{P_i} {\mathbf f}_{o}^{(2)}\left( {\mathbf q}^{(2)}_{i_{p}} \right) 
   + \sum_{t = 1}^{T_i} {\mathbf f}_{o}^{(3)}\left( {\mathbf q}^{(3)}_{i_{t}} \right),

where :math:`p` and :math:`t` run over :math:`P_i` pair and :math:`T_i` triplet interactions. 
Each pair interaction is between a pair of particles, :math:`p=(ab)` and each triplet between 
three particles, :math:`t = (abc)`, where :math:`a`, :math:`b`, :math:`c` are the indices of particles 
in a pair or triplet, :math:`o` is the index of the central particle of configuration :math:`i`, 
in a pair :math:`p` or triplet :math:`t`. The body expansion and the indexing is illustrated in
Fig. :ref:`decomposition-scheme<theory_fig_decomposition>`. The total number of interparticle interactions for sample 
:math:`{\mathbf Q}_i` is thus :math:`M_i = P_i + T_i`.

.. _theory_fig_decomposition:

.. figure:: fig/decomposition.png
   :align: center
   :width: 400

   Left panel: Illustration of the body expansion of a 
   configuration :math:`i` on pairs and triplets, where the reference force 
   :math:`{\mathbf F}(Q_i)` acts on the central particle. The two-body terms 
   are shown by interparticle vectors (green lines), and result in 
   total pairwise force :math:`\sum_p {\mathbf f}^{(2)}({\mathbf q}^{(2)}_{i_p})`.
   A similar decomposition for triplets is used to fit the residual,
   i.e., total minus two-body, force.
   
   Right panel: Representations used for pairs and triplets. :math:`a`,
   :math:`b`, and :math:`c` are the particles of the pair or triplet, and :math:`o` is
   the central particle of the sample, which can be either :math:`a`, :math:`b`,
   or :math:`c`. For triplets, two situations are possible: For obtuse
   triplets, with one edge larger than cutoff, the force on the
   central particle :math:`o=b` is due to the reaction force on particle :math:`a`.
   For acute triplets with all edges below the cutoff, the force on
   the central particle :math:`o=a`, :math:`f_o`, is a sum of two forces, :math:`f_{ac}` and
   :math:`f_{ab}`, which depend on all three angles, :math:`\theta_a`,
   :math:`\theta_b`, and :math:`\theta_c`. For vector differences, we use the
   standard convention,
   :math:`{\mathbf r}_{m_{ab}} =  {\mathbf r}_{m_b} - {\mathbf r}_{m_a}`.
   
Eq. :ref:`decomposition1<theory_eq_decomp1>` formally links the kernels
:math:`\hat K`, :math:`\hat k^{(2)}`, and :math:`\hat k^{(3)}` and can be re-written as:
   
.. math::

   {\mathbf F} = \hat{L} {\mathbf f},

where the mapping matrix :math:`\hat{L} = \left( \mathcal{\hat L}_{i_m} \right)`
is introduced.  Its component, :math:`\mathcal{\hat L}_{i_m}`, is a 
:math:`3 \times 3` identity matrix, :math:`\mathbb{1}_{3 \times 3}`,
if particle environment :math:`i` contains the pair or triplet interaction
:math:`m`, and a :math:`3 \times 3` zero matrix, :math:`\mathbb{0}_{3 \times 3}`,
otherwise. :math:`\hat{L}` is the bookkeeping matrix that connects the :math:`M`
local interactions with the :math:`N` particle environments.
It has dimension :math:`3N \times 3M` due to the three spatial dimensions.
This leads to the following relationship:
:math:`\hat{K} = \hat{L} \hat{k} \hat{L}^\intercal`.

Training a model then corresponds to solving the equation:

.. _theory_eq_learn2:

.. math::

   {\mathbf \alpha} = \left(\hat{K}+\lambda \mathbb{1}\right)^{-1} {\mathbf F} 
   = \left(\hat{L} \hat{k} \hat{L}^\intercal+\lambda \mathbb{1}\right)^{-1} {\mathbf F},

with a regularization term :math:`\lambda` and a :math:`3N`-dimensional vector of
weight coefficients :math:`{\mathbf \alpha}`.

Once trained, the ML model can be used for predictions of the local interactions,
:math:`{\mathbf f}^*`:

.. _theory_eq_learn3:

.. math::

   {\mathbf f}^* = \left(\hat{L}\hat{k}^*\right)^\intercal {\mathbf \alpha},
   
where :math:`\hat{k}^*` denotes
the local kernel between training and test data.   
   
Note that the prediction for entire particle environments yields a 
new mapping matrix :math:`\hat{L}^*` for the test data,

.. _theory_eq_learn4:

.. math::

   {\mathbf F}^* = \hat{L}^* \left(\hat{L} \hat{k}^*\right)^\intercal {\mathbf \alpha}.

Representations
~~~~~~~~~~~~~~~

It is useful to describe local interactions in terms of internal 
coordinates as they are invariant with respect to translations.

For a pair :math:`m` with particles :math:`(a,b)` we use the interparticle distance,

.. _theory_eq_pair:

.. math::

   q_{m}^{(2)} = r_{m_{ab}},

while for a triplet :math:`m` with particles :math:`(a,b,c)`
we use a vector of three interparticle distances,

.. _theory_eq_triplet:

.. math::

   {\mathbf q}^{(3)}_{m} = \left( r_{m_{ab}}, r_{m_{ac}}, r_{m_{bc}} \right)^\intercal \; .

Covariant hessian kernel
~~~~~~~~~~~~~~~~~~~~~~~~

As the target property (force) is a vector, the
kernel has to behave like a second rank tensor upon rotations of
coordinates. Formally, this can be expressed as

.. _theory_eq_cov1:

.. math::

   \mathcal{\hat k}_\textup{c} \left(\mathcal{\hat R}\left({\mathbf q}_m\right),
   \mathcal{\hat R'}\left({\mathbf q}_l\right) \right) = \mathcal{\hat R}\, 
   \mathcal{\hat k}_\textup{c} \left({\mathbf q}_m,{\mathbf q}_l\right)\, 
   \mathcal{\hat R}'^\intercal,

for any two rotation matrices :math:`\mathcal{\hat R}` and
:math:`\mathcal{\hat R'}`. This condition imposes restrictions 
on the form of the kernel :math:`\mathcal{\hat{k}}_\textup{c}`, forcing it to become covariant.

One can construct such a kernel between two forces as a 
hessian of a scalar kernel between two energies:

.. _theory_eq_hessiankernel:

.. math::

   \mathcal{\hat k}_\textup{h} \left({\mathbf q}_m,{\mathbf q}_l\right) =
   \sum_{\alpha,\beta}\frac{\partial^2 k({\mathbf q}_m, {\mathbf q}_l)}{\partial q_{m, \alpha} \partial q_{l, \beta}} 
   \left(\frac{\partial q_{m,\alpha} }{\partial {{\mathbf r}_{m_{o}}}} \right)
   \left(\frac{ \partial q_{l,\beta} }{\partial {{\mathbf r}_{l_{o}}}} \right)^\intercal,

where the second term that contains a first-order partial
derivative of the kernel function is neglected. Here, 
:math:`\alpha` and :math:`\beta` run over the different
components of the representation vector, :math:`o` is the central particle
of the samples, as illustrated in Fig. :ref:`decomposition-scheme<theory_fig_decomposition>`.

Choosing a Gaussian kernel as scalar-valued kernel, for pairwise interactions, 
the representation (Eqn. :ref:`pair<theory_eq_pair>`) results in

.. math::

   \mathcal{{\hat k}}_\textup{h}^{(2)}\left({\mathbf q}_m,{\mathbf q}_l\right) 
   = k^{(2)}(r_{m_{ab}} -r_{l_{ab}})
   \hat{{\mathbf r}}_{m_{ab}} \hat{{\mathbf r}}_{l_{ab}}^\intercal ,

where :math:`k^{(2)}(r)=\left[ \frac{1}{\sigma^2} -\frac{r^2}{\sigma^4} \right]\exp\left(- \frac{r^2}{2\sigma^2} \right)`, 
:math:`\hat{{\mathbf r}}_{m_{ab}} =  (\delta_{{m_o},m_b} - \delta_{{m_o},m_a}) {\mathbf r}_{m_{ab}} / r_{m_{ab}}`, 
and the expression for :math:`\hat{{\mathbf r}}_{l_{ab}}` can be obtained by exchanging 
:math:`m` with :math:`l` in :math:`\hat{{\mathbf r}}_{m_{ab}}`.
Note that the Kronecker deltas only change the sign of the kernel element,
depending on whether the central particle of the sample, :math:`o`, 
coincides with particle :math:`a` or :math:`b`.

For a three-body kernel, the representation (Eqn. :ref:`triplet<theory_eq_triplet>`)
has three components, and the hessian-based kernel becomes a sum over 
the three pair distances in a triplet, 

.. math::

   {\hat k}_h^{(3)}\left({\mathbf q}_m,{\mathbf q}_l\right) = 
   \sum_{p \in ab, ac, bc}
   k_{ml}^{(3)}(r_{m_{p}} -r_{l_{p}})
   \hat{{\mathbf r}}_{m_{p}} \hat{{\mathbf r}}_{l_{p}}^\intercal,

with :math:`k_{ml}^{(3)}(r)=\left[ \frac{1}{\sigma_{p}^2}-\frac{r^2}{\sigma_{p}^4} \right]\exp\left(-\sum_{p} \frac{\left(r_{m_{p}}-r_{l_{p}}\right)^2}{2\sigma_{p}^2}\right)`.

To get the best performing hessian kernel, one has to look at the cutoff scheme.
Only those triangles which have two edges smaller than the cutoff contribute
to the body-expansion. One has to distinguish different cases.
If the central vertex of the triangle, :math:`o`, is linked to an edge longer
that the cutoff (e.g., :math:`bc` in the obtuse triplet shown in
Fig. :ref:`decomposition-scheme<theory_fig_decomposition>`, the force on this particle is only due
to the shorter edge. If all particles are of the same type, one can exploit
this symmetry to enhance the learning efficiency. In this case the triplet 
representation vector :math:`{\mathbf q}^{(3)}_{m}` (Eq. :ref:`triplet<theory_eq_triplet>`)
is sorted with respect to the edge lengths.
For triangles with all three edges below the cutoff,
the force on every vertex is a sum of two forces originating from the neighboring edges, 
as depicted for an acute triplet in Fig. :ref:`decomposition-scheme<theory_fig_decomposition>`.
Each of these forces depends on the three angles of the triangle,
and their ``individual'' representations differ only by permutations
of elements of the representation vector, :math:`{\mathbf q}^{(3)}_{m}` (Eq. :ref:`triplet<theory_eq_triplet>`).
To account for this, in addition to ordering the descriptor,
for acute triplets each of these three permutations is included as
individual local triplet interaction :math:`m` into the mapping
matrix :math:`\mathcal{\hat L}_{i_m}`.
This is the case for triplets where all particles are
of the same type and also where this is not the case.

Cutoffs and switching functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One can enforce the smooth decay of forces to zero at the
cutoff distance :math:`r_{\text{cut}}` via a switching function

.. _theory_eq_cut1:

.. math::

   f_{\text{cut}}\left(r\right) = \begin{cases}
   1, & r \leq r_{\text{cut}}-d \\
   \frac{1}{2} \cos{ \frac{\pi\,\left(r-r_{\text{cut}}+d\right)}{d}} +\frac{1}{2}, & r_{\text{cut}}-d < r < r_{\text{cut}} \\
   0, & r \geq r_{\text{cut}},
   \end{cases}

which goes smoothly to zero in the transition region of
:math:`r_{\text{cut}}-d < r < r_{\text{cut}}`. Each entry of
the mapping matrix :math:`\mathcal{\hat L}_{i_m}` is multiplied
by :math:`f_{\text{cut}}\left(r_{m_{ab}}\right)` for pairs and
:math:`f_{\text{cut}}\left(r_{m_{ab}}\right)f_{\text{cut}}\left(r_{m_{ac}}\right)`
for triplets. This is done for training, :math:`\hat{L}`, as well as prediction, :math:`\hat{L}^*`.

.. _theory_ml_covariant_meshing:

Covariant meshing
~~~~~~~~~~~~~~~~~

The size of the kernel can be bound to :math:`M_{\text{bins}} \times M_{\text{bins}}`
elements by precomputing the pair or triplet kernel :math:`\hat{k}_{\text{bins}}`
on a fixed grid. Due to the covariance of the force kernels, one can choose
an arbitrary reference orientation of the binned pair and triplet representations.
For the two-body kernel, a linear binning is used with :math:`M_{\text{bins}}`
between :math:`r_{\text{min}}` and :math:`r_{\text{max}}`. For the three-body kernel,
a uniform binning on a three-dimensional grid is choosen. The length of the distance vector
:math:`{\mathbf r}^{\text{bins}}_{m_{ab}}` is varied in :math:`M_{r}` steps from
:math:`r^{\text{bins}}_{\text{min}}` to :math:`r^{\text{bins}}_{\text{max}}`. If bead 
:math:`b` and :math:`c` are of the same type, the distance vector
:math:`{\mathbf r}^{\text{bins}}_{m_{ac}}` is varied from length :math:`r^{\text{bins}}_{m_{ab}}`
to :math:`r^{\text{bins}}_{\text{max}}` which is sufficient due to the ordering of the representation.
If not, the distance vector :math:`{\mathbf r}^{\text{bins}}_{m_{ac}}` is also varied from length
:math:`r^{\text{bins}}_{\text{min}}` to :math:`r^{\text{bins}}_{\text{max}}`.
Finally, the angle between the distance vectors :math:`{\mathbf r}^{\text{bins}}_{m_{ab}}` and
:math:`{\mathbf r}^{\text{bins}}_{m_{ac}}`, 
:math:`\theta^{\text{bins}}_{m} = \arccos\left[\left({\mathbf r}^{\text{bins}}_{m_{ab}}
\cdot{\mathbf r}^{\text{bins}}_{m_{ac}}\right)/\left(r^{\text{bins}}_{m_{ab}} r^{\text{bins}}_{m_{ac}}\right) \right]`,
is varied in :math:`M_{\theta}` steps from :math:`\theta^{\text{bins}}_{\text{min}}` to 
:math:`\theta^{\text{bins}}_{\text{max}}`. This implicitly fixes the third
interparticle distance :math:`r^{\text{bins}}_{m_{bc}}`. This means, the total number
of bins for triplets is :math:`M_{\text{bins}}=M_{\theta}M_{r}\left(M_{r}+1\right)/2`
if bead :math:`c` and :math:`b` are of the same type and
:math:`M_{\text{bins}}=M_{\theta}M_{r}M_{r}` if not.

The binned pairs and triplets are rotated to the pairs and triplets of each sample with the
help of the mapping matrix
:math:`\hat{L}_{\text{bins}} = \left( \mathcal{\hat L}^{\text{bins}}_{i_m} \right)`.
The index :math:`i` denotes the :math:`N` configurations and :math:`m` denotes the 
:math:`M_{\text{bins}}` pairs or triplets in the set of binned representations.
Each matrix element :math:`\mathcal{\hat L}^{\text{bins}}_{i_m}` is a
:math:`3 \times 3` matrix,

.. _theory_eq_binning2:

.. math::

   \mathcal{\hat L}^{\text{bins}}_{i_m} = \sum_{i_l} w_{{i_l}m} \mathcal{\hat R}_{{i_l}m},
   
where :math:`i_l`, denotes pairs or triplets of configuration :math:`i`,
:math:`\mathcal{\hat R}_{{i_l}m}` is the rotation matrix that
rotates pair or triplet :math:`i_l` to the fixed orientation of the binned representation :math:`m`.

We apply a Gaussian smearing, :math:`w_{{i_l}m}`, so that a pair or triplet
:math:`i_l` is related to several binned representations. For pairs,

.. _theory_eq_binning3:

.. math::

    w_{{i_l}m}(r_{ab}) = \exp\left[-\frac{\left(r_{i_{l,ab}} 
    -r^{\text{bins}}_{m_{ab}}\right)^2}{2\sigma_r^2}\right]
    f_\text{cut}\left( r_{i_{l,ab}} \right)

where :math:`r_{i_{l,ab}}` and :math:`r^{\text{bins}}_{m_{ab}}`
are pair distances of the actual and binned pairs.
For triplets the weights are a product of three Gaussians,

.. _theory_eq_binning4:

.. math::

   w_{{i_l}m}  = w_{{i_l}m}(r_{ab}) w_{{i_l}m}(r_{ac}) \, w_{{i_l}m}(\theta).

The learning and predicting can be readily derived from 
Eqs. :ref:`learn2<theory_eq_learn2>`, :ref:`learn3<theory_eq_learn3>`, 
and :ref:`learn4<theory_eq_learn4>`.

.. _theory_ml_tabulation:

Tabulated force fields
~~~~~~~~~~~~~~~~~~~~~~

To use the ML force field for MD simulations, the ML model is projected onto tabulated
two- and three-body forces. For two-body forces it is a simple tabulated pair potential
which is automatically written out at the end of the training process. The three-body tabulation
is done on a three-dimensional grid which is constructed in a similar way than for the covariant meshing:
The length of the distance vector :math:`{\mathbf r}^{\text{out}}_{m_{ab}}` is varied in
:math:`M_{r}` steps from :math:`r^{\text{out}}_{\text{min}}` to :math:`r^{\text{out}}_{\text{max}}`.
If bead :math:`b` and :math:`c` are of the same type, the distance vector
:math:`{\mathbf r}^{\text{out}}_{m_{ac}}` is varied from length :math:`r^{\text{out}}_{m_{ab}}`
to :math:`r^{\text{out}}_{\text{max}}` which is sufficient due to the ordering of the representation.
If not, the distance vector :math:`{\mathbf r}^{\text{out}}_{m_{ac}}` is also varied from length
:math:`r^{\text{out}}_{\text{min}}` to :math:`r^{\text{out}}_{\text{max}}`. Finally,
the angle between the distance vectors :math:`{\mathbf r}^{\text{out}}_{m_{ab}}` and
:math:`{\mathbf r}^{\text{out}}_{m_{ac}}`,
:math:`\theta^{\text{out}}_{m} = \arccos\left[\left({\mathbf r}^{\text{out}}_{m_{ab}} 
\cdot{\mathbf r}^{\text{out}}_{m_{ac}}\right)/\left(r^{\text{out}}_{m_{ab}} r^{\text{out}}_{m_{ac}}\right) \right]`,
is varied in :math:`M_{\theta}` steps from :math:`\theta^{\text{out}}_{\text{min}}` to
:math:`\theta^{\text{out}}_{\text{max}}`. This implicitly fixes the third interparticle distance
:math:`r^{\text{out}}_{m_{bc}}`. This means, the total number of bins for triplets is
:math:`M_{\text{out}}=M_{\theta}M_{r}\left(M_{r}+1\right)/2` if bead :math:`c` and
:math:`b` are of the same type and :math:`M_{\text{out}}=M_{\theta}M_{r}M_{r}` if not.

For an efficient tabulation, the three-body interactions need to be further projected onto the interparticle vectors.
The Hessian kernel ensures that the three force vectors on all particles :math:`m_a`, :math:`m_b`,
and :math:`m_c` of each tabulated triplet :math:`m`, :math:`{\mathbf f}_{m_a}`, :math:`{\mathbf f}_{m_b}`,
and :math:`{\mathbf f}_{m_c}`, lie within the plane defined by :math:`{\mathbf r}_{m_{ab}}`,
:math:`{\mathbf r}_{m_{ac}}`, and :math:`{\mathbf r}_{m_{bc}}`. 
This property is used to project the forces onto the inter-particle distance vectors as follows:

.. _theory_eq_ml_tabulation:

.. math::

   \begin{pmatrix}
      {\mathbf f}_{m_a} \\
      {\mathbf f}_{m_b} \\
      {\mathbf f}_{m_c} \\
   \end{pmatrix} =
   \begin{pmatrix}
      f_{m_{a1}} & f_{m_{a2}} & 0 \\
      f_{m_{b1}} & 0 & f_{m_{b2}} \\
      0 & f_{m_{c1}} & f_{m_{c2}} \\
   \end{pmatrix}
   \begin{pmatrix}
      {\mathbf r}_{m_{ab}} \\
      {\mathbf r}_{m_{ac}} \\
      {\mathbf r}_{m_{bc}} \\
   \end{pmatrix}.

Due to symmetry reasons, the following relations hold: :math:`f_{m_{a1}}=-f_{m_{b1}}`,
:math:`f_{m_{a2}}=-f_{m_{c1}}`, and :math:`f_{m_{b2}}=-f_{m_{c2}}`.
For each of the :math:`M_{\text{out}}` triplets, we solve Eq. :ref:`ml_tabulation<theory_eq_ml_tabulation>`
and tabulate the :math:`6` force constants :math:`f_{m_{a1}}`,
:math:`f_{m_{a2}}`, :math:`f_{m_{b1}}`, :math:`f_{m_{b2}}`, :math:`f_{m_{c1}}`,
and :math:`f_{m_{c2}}` allowing for an efficient calculation of the forces
in an MD simulation. Details on the exact table structure are given in section
:ref:`methods_ml_calculating_tabulation`.

