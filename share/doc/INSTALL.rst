.. _install:

Installation
============

To install the full package:

::

    prefix=WHERE/TO/INSTALL/VOTCA
    version=master # or a specific branch 
    git clone -b ${version} https://gitlab.mpcdf.mpg.de/votca/votca.git
    cmake -B builddir -S votca -DCMAKE_INSTALL_PREFIX=${prefix} -DBUILD_CSG_TUTORIALS=ON #example on how to include different CMake Flags
    cmake --build builddir --parallel <number of cores>
    cmake --build builddir --target install



Dependencies     
----------------

``-D`` flag is necessary to specify the path to a missed package. For example, in the case of a locally installed version of
GROMACS:

::

    cmake -DCMAKE_INSTALL_PREFIX=${prefix} -DGROMACS_INCLUDE_DIR=$HOME/gromacs/include -DGROMACS_LIBRARY=$HOME/gromacs/lib/libgromacs.so -S ..

Be careful to use exactly the option suggested in the error message! You
can also add ``-LH`` or ``-LAH`` options to the ``cmake`` command in
order to see the available options with brief explanations (note that
*changing some of the variables may result in more variables being
created*; run ``man cmake`` for more info).

*Only for Linux*: For each dependency package not found by CMake
initially, it might be necessary to add the location of its ``lib``
directory to the environment variable ``LD_LIBRARY_PATH``, **before**
building and installing VOTCA, i.e. before running any ``make`` command.
For example:

::

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/gromacs/lib:$HOME/anaconda/lib

Note that ``LD_LIBRARY_PATH`` also needs to be set every time when
running an executable from the VOTCA installation afterwards (which can
be automated via the user's login profile, e.g. in .bashrc). Alternatively,
CMake has options to *remember* where libraries came from at link time,
which can be enabled by setting ``CMAKE_INSTALL_RPATH_USE_LINK_PATH`` to
``ON``. VOTCA has enabled this option and a couple of other rpath
related tweaks when setting ``ENABLE_RPATH_INJECT`` to ``ON``.

Common CMake Flags
~~~~~~~~~~~~~~~~~~

-  ``BUILD_TOOLS`` - Build the tools repo (ON/OFF, Default ON)
-  ``BUILD_CSG`` - Build the csg repo (ON/OFF, Default ON)
-  ``BUILD_CTP`` - Build the ctp repo (ON/OFF, Default ON)
-  ``CMAKE_INSTALL_PREFIX`` - where to install the votca executables
-  ``BUILD_CSG_MANUAL`` - Build the csg manual as .pdf version. It is then written into ${prefix}/share/doc/votca/csg-manual.pdf (ON/OFF, Default OFF)
-  ``BUILD_CTP_MANUAL`` - Build the ctp manual as .pdf version. It is then written into ${prefix}/share/doc/votca/ctp-manual.pdf (ON/OFF, Default OFF)
-  ``BUILD_CSG_TUTORIALS`` - Build the csg-tutorials. They are then written into ${prefix}/share/votca/csg-tutorials (ON/OFF, Default OFF)
-  ``BUILD_CSGAPPS`` - Install the extra csg applications repo (ON/OFF, Default OFF)
-  ``ENABLE_TESTING`` - compile tests (ON/OFF, Default OFF)
-  ``BUILD_SHARED_LIBS`` - Build shared libs (ON/OFF, Default ON)

Performance advice
~~~~~~~~~~~~~~~~~~
A lot of performance can be gained by enabling vectorization and/or use of Intel's ``MKL`` 
as backend, which is automatically detected by ``CMake``. The apps csg_fmatch and csg_ml of VOTCA-CSG rely on the `Eigen <https://eigen.tuxfamily.org>`_ library for vector-matrix operations. To use the OPENMP parallelization of the Eigen library ``-DCMAKE_CXX_FLAGS=-fopenmp`` is automatically set.

Below are some recommendations
for different architectures:

Intel Processors
^^^^^^^^^^^^^^^^
``g++``, ``clang``, and ``ipcx`` from the Intel OneAPI basekit give similar performance 
when used with the MKL. No special flags have to be supplied to ``CMake``.

If ``g++`` or ``clang`` are used, the compiler option ``-march=native`` is automatically injected
into the build. If you compile VOTCA on a heterogeneous cluster with different instruction sets,
this may cause the executables to not run. Override this by specifying 
``-DCMAKE_CXX_FLAGS=-mtune=native`` (at probably lower performance), or perform node-type 
specific builds.

As a rough estimate, runtimes with vectorization and  ``gcc/clang`` are 30% shorter than without
vectorization. Use of ``MKL`` reduces them by another 50%. 

AMD Processors
^^^^^^^^^^^^^^
We recommend using ``g++`` or ``clang`` rather than an Intel compiler on AMD. Vectorization 
in ``Eigen`` is automatically enabled by injection of ``-march=native``. See above comment
about heterogeneous envionments. 

If you have Intel's MKL installed, and it is found by ``CMake``, performance will be helped 
but by how much idepends on which architecture-specific implementation MKL picks at runtime. 
This can be affected by a `vendor lock-in <https://en.wikipedia.org/wiki/Math_Kernel_Library>`__, 
and work-arounds are documented `for Intel MKL on AMD Zen <https://danieldk.eu/Posts/2020-08-31-MKL-Zen.html>`__ 
and `in general <https://documentation.sigma2.no/jobs/mkl.html#using-mkl-efficiently>`__. 
We advise that you test this on your specific architecture.

GCC and MKL : undefined symbol:
-------------------------------

This can happen with some GCC versions. Adding the
``-Wl,--no-as-needed`` to ``CMAKE_EXE_LINKER_FLAGS:STRING=`` to the
``CMakeCache.txt`` in the ``build`` directory can fix this. For more
information look
`here <https://software.intel.com/en-us/articles/symbol-lookup-error-when-linking-intel-mkl-with-gcc-on-ubuntu>`__
