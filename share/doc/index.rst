VOTCA documentation
=================================

.. toctree::
   :maxdepth: 3
   :hidden:
     
   INSTALL.rst

.. toctree::
   :maxdepth: 2
   :hidden:

   csg/index.rst
   ctp/index.rst

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Tutorials

   csg-tutorials/README.rst
..   ctp-tutorials/README.rst
   

VOTCA is a package that helps to design multiscale models for molecular materials. It is open-source and can be used to analyze molecular dynamics data, parameterize coarsep-grained force fields, calculate the density of states of organic semiconductors, evaluate the rates of charge and energy transfer reactions, and perform kinetic Monte Carlo simulations.
