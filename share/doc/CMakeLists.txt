#set(DOXYGEN_SOURCES)
#foreach(DIR ${ENABLED_VOTCA_PACKAGES})
#  set(ABS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../${DIR})
#  set(INPUT_DIRS "${INPUT_DIRS} ${ABS_DIR}")
#  file(GLOB_RECURSE ${DIR}_SOURCES ${ABS_DIR}/*.cc ${ABS_DIR}/*.h
#         ${ABS_DIR}/*.dox ${ABS_DIR}/guide/*.md)
#  list(APPEND DOXYGEN_SOURCES ${${DIR}_SOURCES})
#endforeach()
#file(GLOB EXTRA_DOXYGEN_PAGES ${CMAKE_CURRENT_SOURCE_DIR}/*.md ${CMAKE_SOURCE_DIR}/*.md)
#list(APPEND DOXYGEN_SOURCES ${EXTRA_DOXYGEN_PAGES})
#string(REPLACE ";" " " INPUT_SOURCES "${DOXYGEN_SOURCES}")
#configure_file(Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
#add_custom_target(doxygen DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/html/index.html)
#add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/html/index.html
#  COMMAND ${DOXYGEN_EXECUTABLE} Doxyfile
#  DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile ${DOXYGEN_SOURCES}
#  COMMENT "Build doxygen documentation")

include(FindPackageHandleStandardArgs)

find_program(SPHINX_EXECUTABLE NAMES sphinx-build DOC "Sphinx documentation generation tool (http://www.sphinx-doc.org/)")
find_package_handle_standard_args(SPHINX REQUIRED_VARS SPHINX_EXECUTABLE)
if(SPHINX_FOUND)
  set(SPHINX_DEPS ${VOTCA_SPHINX_DIR}/conf.py)
  file(GLOB PAGES *.rst)
  file(GLOB IMAGES *.png)
  configure_file(conf.py.in ${VOTCA_SPHINX_DIR}/conf.py @ONLY)
  foreach(_DEP ${PAGES} ${IMAGES})
    get_filename_component(_FILE "${_DEP}" NAME)
    add_custom_command(OUTPUT ${VOTCA_SPHINX_DIR}/${_FILE}
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${_DEP} ${VOTCA_SPHINX_DIR}/${_FILE} DEPENDS ${_DEP}
    )
    list(APPEND SPHINX_DEPS ${VOTCA_SPHINX_DIR}/${_FILE})
  endforeach()
  file(COPY _themes DESTINATION ${VOTCA_SPHINX_DIR})
  add_custom_target(doc COMMAND ${SPHINX_EXECUTABLE} ${VOTCA_SPHINX_DIR} ${VOTCA_SPHINX_OUTPUT_DIR}
    DEPENDS ${SPHINX_DEPS})
  foreach(_MODULE ${ENABLED_VOTCA_PACKAGES})
    if(TARGET doc-${_MODULE})
      add_dependencies(doc doc-${_MODULE})
    endif()
  endforeach()
endif()
