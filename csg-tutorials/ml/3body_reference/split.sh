#!/bin/bash

#splits the 10000 frame trajcetory into 10 chunks of 1000 frames each
#each frame consists of 1000 atoms and 1009 lines in the dump file (1000*1009=1009000)

mkdir trajectory_split
cd trajectory_split

for i in {1..10..1}
do	
  head -n "$(($i*1009000))" ../spce_rerun_3body.dump | tail -n 1009000 > spce_rerun_3body_$i.dump
done
