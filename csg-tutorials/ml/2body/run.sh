#! /bin/bash -e

#cp two independent parts of the reference lj trajectory as train.dump and test.dump
cp ../2body_reference/trajectory_split/lj_1.dump train.dump
cp ../2body_reference/trajectory_split/lj_2.dump test.dump
