#! /bin/bash -e

#run the LAMMPS simulation (needs a current LAMMPS version compiled)
lmp < lj.in > lj.out

#run script to split the trajectory into 10 parts
./split.sh
