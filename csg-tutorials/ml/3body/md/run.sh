#! /bin/bash -e

#copy the 3-body force table from the 3-body ML tutorial (should be changed to actual ML model used)
cp ../with_binning/1-1-1.table .

#run the LAMMPS simulation (needs a current LAMMPS version compiled with the user pair_style 3b/table)
lmp < ml.in > ml.out
