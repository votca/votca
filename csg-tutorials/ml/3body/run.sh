#! /bin/bash -e

#cp two independent parts of the reference (rerun) spce trajectory as train.dump and test.dump
cp ../3body_reference/trajectory_split/spce_rerun_3body_1.dump train.dump
cp ../3body_reference/trajectory_split/spce_rerun_3body_2.dump test.dump
