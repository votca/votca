#! /bin/bash -e

#create .tpr file for coarse-grained configuration
gmx grompp -c confout_cg.gro -p topol_cg.top -f sample_cg.mdp -n index.ndx -o sample_cg.tpr

#create .tpr file for atomistic reference configuration
gmx grompp -c conf_atomistic.gro -f sample_atomistic.mdp -p topol_atomistic.top -o sample_atomistic.tpr

#run csg_backmap
csg_backmap --no-map --top sample_cg.tpr --trj confout_cg.gro --out confout_hybrid.gro --cg-map TMBT_two_beadtypes.xml --top-atom sample_atomistic.tpr --trj-atom conf_atomistic.gro


#take the hybrid configuration of the csg_backmap output (../confout_hybrid.gro) as input configuration and also as restraint coordinates ( -r)
gmx grompp -c confout_hybrid.gro -p topol_hybrid_only_bonded_no_impropers.top -f energymin.mdp -po energyminout_only_bonded_no_impropers.mdp -o energymin_only_bonded_no_impropers.tpr -r confout_hybrid.gro -maxwarn 2

#run energy minimization only with bonded interactions and no impropers with position restraints
gmx mdrun -s energymin_only_bonded_no_impropers.tpr -o energymin_only_bonded_no_impropers.trr -x energymin_only_bonded_no_impropers.xtc -e energymin_only_bonded_no_impropers.edr -g energymin_only_bonded_no_impropers.log -c confout_only_bonded_no_impropers.gro

#take the output configuration of the energy minimization without impropers as input configuration for the minimization with impropers (confout_only_bonded_no_impropers.gro) and take again the output of the csg_backmap output (../confout_hybrid.gro) as restraint coordinates ( -r)
gmx grompp -c confout_only_bonded_no_impropers.gro -p topol_hybrid_only_bonded.top -f energymin.mdp -po energyminout_only_bonded.mdp -o energymin_only_bonded.tpr -r confout_hybrid.gro -maxwarn 2

#run energy minimization only with bonded interactions with position restraints
gmx mdrun -s energymin_only_bonded.tpr -o energymin_only_bonded.trr -x energymin_only_bonded.xtc -e energymin_only_bonded.edr -g energymin_only_bonded.log -c confout_only_bonded.gro


# here one has to use a double precision gromacs version!

#take the output configuration of the energy minimization with impropers as input configuration for the minimization with impropers (confout_only_bonded.gro) and take again the output of the csg_backmap output (../confout_hybrid.gro) as restraint coordinates ( -r)

#run this energy minimization with GROMACS in double precision!

gmx_d grompp -c confout_only_bonded.gro -p topol_hybrid.top -f energymin_PME.mdp -po energyminout_bonded_and_nonbonded.mdp -o energymin_bonded_and_nonbonded.tpr -r confout_hybrid.gro -maxwarn 2

#run energy minimization with bonded and non-bonded interactions, still with position restraints 
gmx_d mdrun -s energymin_bonded_and_nonbonded.tpr -o energymin_bonded_and_nonbonded.trr -x energymin_bonded_and_nonbonded.xtc -e energymin_bonded_and_nonbonded.edr -g energymin_bonded_and_nonbonded.log -c confout_bonded_and_nonbonded.gro


#take the output configuration of the energy minimization with still the position restraints on as input configuration for the minimization without restraints (../confout.gro)

#run this energy minimization again with GROMACS in single precision!

gmx grompp -c confout_bonded_and_nonbonded.gro -p topol_hybrid_no_restraints.top -f energymin_PME.mdp -po energyminout_without_restraints.mdp -o energymin_without_restraints.tpr -maxwarn 2

#run energy minimization with bonded and non-bonded interactions without position restraints
gmx mdrun -s energymin_without_restraints.tpr -o energymin_without_restraints.trr -x energymin_without_restraints.xtc -e energymin_without_restraints.edr -g energymin_without_restraints.log -c confout_without_restraints.gro

