|CI| |coverage report|

VOTCA 
###################
Simulation package for 
 - systematic coarse-graining for morphology simulations 
 - polarizable force-fields for density of states and refractive index calculations
 - kinetic Monte Carlo simulations of charge transport
 - overlap of electronic wavefunctions for charge transfer rates
 
Basic installation 
###################
(see performance advice in `Install Guide <share/doc/INSTALL.rst>`__)

::

    prefix=WHERE/TO/INSTALL/VOTCA
    version=master #or a specific branch
    git clone -b ${version} https://gitlab.mpcdf.mpg.de/votca/votca.git
    cmake -B builddir -S votca -DCMAKE_INSTALL_PREFIX=${prefix}
    cmake --build builddir --parallel <number of cores>
    cmake --build builddir --target install

More detailed information:
##########################

1. `Installation <share/doc/INSTALL.rst>`__
2. `Developers Guide <share/doc/DEVELOPERS_GUIDE.rst>`__
3. `VOTCA\_LANGUAGE\_GUIDE <share/doc/VOTCA_LANGUAGE_GUIDE.rst>`__
4. `Code of Conduct <share/doc/CODE_OF_CONDUCT.rst>`__

.. |CI| image:: https://gitlab.mpcdf.mpg.de/votca/votca/badges/master/pipeline.svg
   :target: https://gitlab.mpcdf.mpg.de/votca/votca/commits/master
.. |coverage report| image:: https://gitlab.mpcdf.mpg.de/votca/votca/badges/master/coverage.svg
   :target: https://gitlab.mpcdf.mpg.de/votca/votca/commits/master

License:
########

Copyright 2009-2025 The VOTCA Development Team


Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

::

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
